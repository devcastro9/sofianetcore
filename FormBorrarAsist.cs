﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormBorrarAsist : Form
    {
        private readonly AppDbContext _db;
        public FormBorrarAsist(AppDbContext appDb)
        {
            InitializeComponent();
            _db = appDb;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void FormBorrarAsist_Load(object sender, EventArgs e)
        {
            dtAsistencia.Value = DateTime.Now;
            ComboPlanilla.DataSource = await _db.RcPlanillaGrupos.AsNoTracking().Select(m => new { Id = m.PlanillaCodigo, Desc = m.PlanillaCodigo + "-" + m.PlanillaDescripcion }).ToListAsync();
            ComboPlanilla.DisplayMember = "Desc";
            ComboPlanilla.ValueMember = "Id";
        }
        private async void ButtonBorrar_Click(object sender, EventArgs e)
        {
            _ = await _db.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[rp_borrar_asistencia] {dtAsistencia.Value.Year}, {dtAsistencia.Value.Month}, {ComboPlanilla.SelectedValue}");
            MessageBox.Show("Asistencias borrados", "Asistencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
