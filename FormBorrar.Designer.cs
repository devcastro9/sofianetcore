﻿namespace SofiaNetCore
{
    partial class FormBorrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.IconExit = new FontAwesome.Sharp.IconButton();
            this.ButtonImportar = new FontAwesome.Sharp.IconButton();
            this.ComboBancos = new System.Windows.Forms.ComboBox();
            this.DataBancos = new System.Windows.Forms.DataGridView();
            this.ButtonBorrar = new FontAwesome.Sharp.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.DataBancos)).BeginInit();
            this.SuspendLayout();
            // 
            // IconExit
            // 
            this.IconExit.FlatAppearance.BorderSize = 0;
            this.IconExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconExit.IconChar = FontAwesome.Sharp.IconChar.Multiply;
            this.IconExit.IconColor = System.Drawing.Color.White;
            this.IconExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconExit.IconSize = 30;
            this.IconExit.Location = new System.Drawing.Point(708, 13);
            this.IconExit.Name = "IconExit";
            this.IconExit.Size = new System.Drawing.Size(30, 33);
            this.IconExit.TabIndex = 0;
            this.IconExit.UseVisualStyleBackColor = true;
            this.IconExit.Click += new System.EventHandler(this.IconExit_Click);
            // 
            // ButtonImportar
            // 
            this.ButtonImportar.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ButtonImportar.FlatAppearance.BorderSize = 0;
            this.ButtonImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonImportar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ButtonImportar.ForeColor = System.Drawing.Color.White;
            this.ButtonImportar.IconChar = FontAwesome.Sharp.IconChar.FileUpload;
            this.ButtonImportar.IconColor = System.Drawing.Color.White;
            this.ButtonImportar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonImportar.IconSize = 30;
            this.ButtonImportar.Location = new System.Drawing.Point(78, 43);
            this.ButtonImportar.Name = "ButtonImportar";
            this.ButtonImportar.Size = new System.Drawing.Size(172, 49);
            this.ButtonImportar.TabIndex = 4;
            this.ButtonImportar.Text = "SELECCIONAR";
            this.ButtonImportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonImportar.UseVisualStyleBackColor = false;
            this.ButtonImportar.Click += new System.EventHandler(this.ButtonImportar_Click);
            // 
            // ComboBancos
            // 
            this.ComboBancos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ComboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBancos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboBancos.ForeColor = System.Drawing.Color.White;
            this.ComboBancos.FormattingEnabled = true;
            this.ComboBancos.Location = new System.Drawing.Point(286, 54);
            this.ComboBancos.Name = "ComboBancos";
            this.ComboBancos.Size = new System.Drawing.Size(210, 31);
            this.ComboBancos.TabIndex = 5;
            this.ComboBancos.SelectedValueChanged += new System.EventHandler(this.ComboBancos_SelectedValueChanged);
            // 
            // DataBancos
            // 
            this.DataBancos.AllowUserToAddRows = false;
            this.DataBancos.AllowUserToDeleteRows = false;
            this.DataBancos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.DataBancos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataBancos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataBancos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataBancos.ColumnHeadersHeight = 30;
            this.DataBancos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(42)))), ((int)(((byte)(83)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataBancos.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataBancos.EnableHeadersVisualStyles = false;
            this.DataBancos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
            this.DataBancos.Location = new System.Drawing.Point(50, 131);
            this.DataBancos.Name = "DataBancos";
            this.DataBancos.ReadOnly = true;
            this.DataBancos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.DataBancos.RowHeadersVisible = false;
            this.DataBancos.RowHeadersWidth = 51;
            this.DataBancos.RowTemplate.Height = 25;
            this.DataBancos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataBancos.Size = new System.Drawing.Size(650, 380);
            this.DataBancos.TabIndex = 6;
            // 
            // ButtonBorrar
            // 
            this.ButtonBorrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ButtonBorrar.FlatAppearance.BorderSize = 0;
            this.ButtonBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBorrar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ButtonBorrar.ForeColor = System.Drawing.Color.White;
            this.ButtonBorrar.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.ButtonBorrar.IconColor = System.Drawing.Color.White;
            this.ButtonBorrar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonBorrar.IconSize = 30;
            this.ButtonBorrar.Location = new System.Drawing.Point(532, 43);
            this.ButtonBorrar.Name = "ButtonBorrar";
            this.ButtonBorrar.Size = new System.Drawing.Size(140, 49);
            this.ButtonBorrar.TabIndex = 7;
            this.ButtonBorrar.Text = "BORRAR";
            this.ButtonBorrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonBorrar.UseVisualStyleBackColor = false;
            this.ButtonBorrar.Click += new System.EventHandler(this.ButtonBorrar_Click);
            // 
            // FormBorrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(750, 550);
            this.Controls.Add(this.ButtonBorrar);
            this.Controls.Add(this.DataBancos);
            this.Controls.Add(this.ComboBancos);
            this.Controls.Add(this.ButtonImportar);
            this.Controls.Add(this.IconExit);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBorrar";
            this.Text = "FormCsv";
            this.Load += new System.EventHandler(this.FormBorrar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataBancos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton IconExit;
        private FontAwesome.Sharp.IconButton ButtonImportar;
        private System.Windows.Forms.ComboBox ComboBancos;
        private System.Windows.Forms.DataGridView DataBancos;
        private FontAwesome.Sharp.IconButton ButtonBorrar;
    }
}