﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormBorrar : Form
    {
        private readonly AppDbContext _db;
        private bool estado = false;
        private string? ArchivoNombre;
        public FormBorrar(AppDbContext appDb)
        {
            InitializeComponent();
            _db = appDb;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ButtonImportar_Click(object sender, EventArgs e)
        {
            string? cuenta = ComboBancos.SelectedValue!.ToString();
            string? banco = ObtenerBanco(cuenta);
            if (string.IsNullOrEmpty(cuenta) || string.IsNullOrEmpty(banco))
            {
                return;
            }
            using OpenFileDialog archive = new();
            archive.Title = "Importar Excel para borrar";
            archive.InitialDirectory = @"C:\\";
            archive.Filter = "Excel 97-2003 (*.xls)|*.xls|Libro de Excel (*.xlsx)|*.xlsx";
            archive.FilterIndex = 0;
            archive.RestoreDirectory = true;
            if (archive.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(archive.FileName))
                {
                    ArchivoNombre = Path.GetFileNameWithoutExtension(archive.FileName);
                    ComboBancos.Visible = true;
                    ActualizarDatos();
                }
            }
            else
            {
                ComboBancos.Visible = !string.IsNullOrEmpty(ArchivoNombre);
                ArchivoNombre = string.Empty;
            }
        }
        private async void ButtonBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ArchivoNombre))
            {
                return;
            }
            string? cuenta = ComboBancos.SelectedValue!.ToString();
            string? banco = ObtenerBanco(cuenta);
            if (string.IsNullOrEmpty(cuenta) || string.IsNullOrEmpty(banco))
            {
                return;
            }
            FormattableString query;
            switch (banco)
            {
                case "BMSC":
                    query = $"DELETE FROM [dbo].[fo_extracto_BMSC_202101] WHERE [nombre_archivo] = {ArchivoNombre} AND [Cuenta] = {cuenta} AND [estado_conciliado] = 'REG'";
                    break;
                case "BUNION":
                    query = $"DELETE FROM [dbo].[fo_extracto_BUNION_202101] WHERE [nombre_archivo] = {ArchivoNombre} AND [Cuenta] = {cuenta} AND [estado_conciliado] = 'REG'";
                    break;
                case "BCP":
                    query = $"DELETE FROM [dbo].[fo_extracto_BCP] WHERE [nombre_archivo] = {ArchivoNombre} AND [Cuenta] = {cuenta} AND [estado_conciliado] = 'REG'";
                    break;
                case "BNB":
                    query = $"DELETE FROM [dbo].[fo_extracto_BNB] WHERE [nombre_archivo] = {ArchivoNombre} AND [Cuenta] = {cuenta} AND [estado_conciliado] = 'REG'";
                    break;
                case "BGA":
                    query = $"DELETE FROM [dbo].[fo_extracto_BGA] WHERE [nombre_archivo] = {ArchivoNombre} AND [Cuenta] = {cuenta} AND [estado_conciliado] = 'REG'";
                    break;
                default:
                    return;
            }
            await _db.Database.ExecuteSqlInterpolatedAsync(query);
            ActualizarDatos();
        }
        private void FormBorrar_Load(object sender, EventArgs e)
        {
            // Carga de Valores por defecto
            ButtonBorrar.Visible = false;
            DataBancos.DataSource = null;
            // Objeto de carga de datos
            ComboBancos.DataSource = _db.FvExtractoCuentas.AsNoTracking().ToList();
            ComboBancos.DisplayMember = "Descripcion";
            ComboBancos.ValueMember = "cuenta";
            ComboBancos.SelectedIndex = 0;
            ComboBancos.Visible = false;
            // Cambio de estado
            estado = true;
        }
        private void ComboBancos_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!estado)
            {
                return;
            }
            ActualizarDatos();
        }
        private async void ActualizarDatos()
        {
            if (!estado)
            {
                return;
            }
            string? Cuenta = ComboBancos.SelectedValue!.ToString();
            string? Banco = ObtenerBanco(Cuenta);
            if (string.IsNullOrEmpty(Cuenta) || string.IsNullOrEmpty(Banco) || string.IsNullOrEmpty(ArchivoNombre))
            {
                return;
            }
            switch (Banco)
            {
                case "BMSC":
                    List<FoExtractoBmsc202101> ResultBMSC = await _db.FoExtractoBmsc202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.NombreArchivo == ArchivoNombre).ToListAsync();
                    if (ResultBMSC.Count > 0)
                    {
                        ButtonBorrar.Visible = true;
                        DataBancos.DataSource = ResultBMSC;
                    }
                    else
                    {
                        DataBancos.DataSource = null;
                    }
                    break;
                case "BUN":
                    List<FoExtractoBunion202101> ResultBUN = await _db.FoExtractoBunion202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.NombreArchivo == ArchivoNombre).ToListAsync();
                    if (ResultBUN.Count > 0)
                    {
                        ButtonBorrar.Visible = true;
                        DataBancos.DataSource = ResultBUN;
                    }
                    else
                    {
                        DataBancos.DataSource = null;
                    }
                    break;
                case "BCP":
                    List<FoExtractoBcp> ResultBCP = await _db.FoExtractoBcps.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.NombreArchivo == ArchivoNombre).ToListAsync();
                    if (ResultBCP.Count > 0)
                    {
                        ButtonBorrar.Visible = true;
                        DataBancos.DataSource = ResultBCP;
                    }
                    else
                    {
                        DataBancos.DataSource = null;
                    }
                    break;
                case "BNB":
                    List<FoExtractoBnb> ResultBNB = await _db.FoExtractoBnbs.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.NombreArchivo == ArchivoNombre).ToListAsync();
                    if (ResultBNB.Count > 0)
                    {
                        ButtonBorrar.Visible = true;
                        DataBancos.DataSource = ResultBNB;
                    }
                    else
                    {
                        DataBancos.DataSource = null;
                    }
                    break;
                case "BGA":
                    List<FoExtractoBga> ResultBGA = await _db.FoExtractoBgas.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.NombreArchivo == ArchivoNombre).ToListAsync();
                    if (ResultBGA.Count > 0)
                    {
                        ButtonBorrar.Visible = true;
                        DataBancos.DataSource = ResultBGA;
                    }
                    else
                    {
                        DataBancos.DataSource = null;
                    }
                    break;
                default:
                    ButtonBorrar.Visible = false;
                    DataBancos.DataSource = null;
                    break;
            }
        }
        private string? ObtenerBanco(string? cuenta)
        {
            if (!estado)
            {
                return null;
            }
            if (cuenta == null)
            {
                return null;
            }
            string? BancoCodigo = (from bco in _db.FvExtractoCuentas where bco.Cuenta == cuenta select bco.Banco).FirstOrDefault();
            if (BancoCodigo == null)
            {
                return null;
            }
            return BancoCodigo;
        }
    }
}
