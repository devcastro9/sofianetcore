﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SofiaNetCore.Utility
{
    public static class UFunciones
    {
        // Verificacion de formato
        public static bool VerificarFormato(string? cadena) => !string.IsNullOrEmpty(cadena) && new Regex(@"^[0-9]{2}/[0-9]{2}/[0-9]{4}$").IsMatch(cadena);
        // Recortar String
        public static string Recort(string? cadena) => !string.IsNullOrEmpty(cadena) ? cadena.Trim(' ') : "";
        // Recortar a decimal
        public static decimal RecortDec(string? cadena) => !string.IsNullOrEmpty(cadena) ? Convert.ToDecimal(cadena.Trim(' '), new CultureInfo("en-US")) : 0;
        // Convertir a decimal
        public static decimal? ToDec(string? cadena) => !string.IsNullOrEmpty(cadena) ? Convert.ToDecimal(cadena.Trim(' ')) : null;
        // Convertir a Datetime
        public static DateTime? ToDTime(string? cadena) => !string.IsNullOrEmpty(cadena) ? Convert.ToDateTime(cadena) : null;
        // TimeSpan
        public static TimeSpan? ToTime(string? cadena) => !string.IsNullOrEmpty(cadena) ? TimeSpan.Parse(cadena) : null;
        // Boolean
        public static bool? ToBoolean(string? cadena) => !string.IsNullOrEmpty(cadena) ? Convert.ToBoolean(cadena) : null;
        // Normalizar nombre de Extracto
        public static string NormalizarExtracto(bool EsMes, DateTime fecha, string cuenta) => EsMes ? $"EB_{fecha:yyyyMM}_{cuenta}" : $"EB_{fecha:yyyyMMdd}_{cuenta}";
        // Normalizar nombre de Asistencia
        public static string NormalizarAsistencia(bool EsMes, DateTime fecha, string equipo) => EsMes ? $"ASIST_{fecha:yyyyMM}_{equipo}" : $"ASIST_{fecha:yyyyMMdd}_{equipo}";
        // Extraer el numero de cuenta del banco sol
        public static string? ExtractNumber(string input)
        {
            string pattern = @"\d+-\d+-\d+";
            Match match = Regex.Match(input, pattern);
            return match.Success ? match.Value.Replace("-", "") : null;
        }
        // Valida el formato de fecha del Banco Sol
        public static bool VerificarFormato_Bsol(string? hora)
        {
            // Verifica si la cadena es nula o vacía
            if (string.IsNullOrWhiteSpace(hora))
            {
                return false;
            }
            // Intenta parsear el valor como TimeSpan
            return TimeSpan.TryParse(hora, out _);
        }
    }
}
