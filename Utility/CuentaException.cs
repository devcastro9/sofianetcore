﻿using System;

namespace SofiaNetCore.Utility
{
    public class CuentaException : Exception
    {
        public CuentaException(string message) : base(message) { }
    }
}
