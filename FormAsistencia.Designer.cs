﻿namespace SofiaNetCore
{
    partial class FormAsistencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.IconExit = new FontAwesome.Sharp.IconButton();
            this.ButtonImportar = new FontAwesome.Sharp.IconButton();
            this.dtAsistencia = new System.Windows.Forms.DateTimePicker();
            this.PorMes = new System.Windows.Forms.RadioButton();
            this.PorDia = new System.Windows.Forms.RadioButton();
            this.ComboEquipos = new System.Windows.Forms.ComboBox();
            this.DataAsistencias = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataAsistencias)).BeginInit();
            this.SuspendLayout();
            // 
            // IconExit
            // 
            this.IconExit.FlatAppearance.BorderSize = 0;
            this.IconExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconExit.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.IconExit.IconChar = FontAwesome.Sharp.IconChar.Multiply;
            this.IconExit.IconColor = System.Drawing.Color.White;
            this.IconExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconExit.IconSize = 30;
            this.IconExit.Location = new System.Drawing.Point(708, 12);
            this.IconExit.Name = "IconExit";
            this.IconExit.Size = new System.Drawing.Size(30, 30);
            this.IconExit.TabIndex = 6;
            this.IconExit.UseVisualStyleBackColor = true;
            this.IconExit.Click += new System.EventHandler(this.IconExit_Click);
            // 
            // ButtonImportar
            // 
            this.ButtonImportar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ButtonImportar.FlatAppearance.BorderSize = 0;
            this.ButtonImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonImportar.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonImportar.ForeColor = System.Drawing.Color.White;
            this.ButtonImportar.IconChar = FontAwesome.Sharp.IconChar.FileUpload;
            this.ButtonImportar.IconColor = System.Drawing.Color.White;
            this.ButtonImportar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonImportar.IconSize = 30;
            this.ButtonImportar.Location = new System.Drawing.Point(522, 43);
            this.ButtonImportar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ButtonImportar.Name = "ButtonImportar";
            this.ButtonImportar.Size = new System.Drawing.Size(140, 50);
            this.ButtonImportar.TabIndex = 7;
            this.ButtonImportar.Text = "IMPORTAR";
            this.ButtonImportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonImportar.UseVisualStyleBackColor = false;
            this.ButtonImportar.Click += new System.EventHandler(this.ButtonImportar_Click);
            // 
            // dtAsistencia
            // 
            this.dtAsistencia.CalendarFont = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtAsistencia.CustomFormat = "";
            this.dtAsistencia.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtAsistencia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAsistencia.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dtAsistencia.Location = new System.Drawing.Point(69, 74);
            this.dtAsistencia.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtAsistencia.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtAsistencia.Name = "dtAsistencia";
            this.dtAsistencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtAsistencia.Size = new System.Drawing.Size(145, 30);
            this.dtAsistencia.TabIndex = 3;
            this.dtAsistencia.Value = new System.DateTime(2011, 12, 26, 15, 32, 0, 0);
            this.dtAsistencia.ValueChanged += new System.EventHandler(this.dtAsistencia_ValueChanged);
            // 
            // PorMes
            // 
            this.PorMes.AutoSize = true;
            this.PorMes.Checked = true;
            this.PorMes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PorMes.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PorMes.Location = new System.Drawing.Point(72, 25);
            this.PorMes.Name = "PorMes";
            this.PorMes.Size = new System.Drawing.Size(142, 27);
            this.PorMes.TabIndex = 1;
            this.PorMes.TabStop = true;
            this.PorMes.Text = "Carga por mes";
            this.PorMes.UseVisualStyleBackColor = true;
            this.PorMes.CheckedChanged += new System.EventHandler(this.PorMes_CheckedChanged);
            // 
            // PorDia
            // 
            this.PorDia.AutoSize = true;
            this.PorDia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PorDia.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PorDia.Location = new System.Drawing.Point(269, 25);
            this.PorDia.Name = "PorDia";
            this.PorDia.Size = new System.Drawing.Size(134, 27);
            this.PorDia.TabIndex = 2;
            this.PorDia.Text = "Carga por día";
            this.PorDia.UseVisualStyleBackColor = true;
            this.PorDia.CheckedChanged += new System.EventHandler(this.PorDia_CheckedChanged);
            // 
            // ComboEquipos
            // 
            this.ComboEquipos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ComboEquipos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboEquipos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboEquipos.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ComboEquipos.ForeColor = System.Drawing.Color.White;
            this.ComboEquipos.FormattingEnabled = true;
            this.ComboEquipos.Location = new System.Drawing.Point(247, 73);
            this.ComboEquipos.Margin = new System.Windows.Forms.Padding(0);
            this.ComboEquipos.Name = "ComboEquipos";
            this.ComboEquipos.Size = new System.Drawing.Size(196, 31);
            this.ComboEquipos.TabIndex = 9;
            this.ComboEquipos.SelectedValueChanged += new System.EventHandler(this.ComboEquipos_SelectedValueChanged);
            // 
            // DataAsistencias
            // 
            this.DataAsistencias.AllowUserToAddRows = false;
            this.DataAsistencias.AllowUserToDeleteRows = false;
            this.DataAsistencias.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.DataAsistencias.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataAsistencias.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataAsistencias.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataAsistencias.ColumnHeadersHeight = 30;
            this.DataAsistencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(42)))), ((int)(((byte)(83)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataAsistencias.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataAsistencias.EnableHeadersVisualStyles = false;
            this.DataAsistencias.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
            this.DataAsistencias.Location = new System.Drawing.Point(50, 137);
            this.DataAsistencias.Name = "DataAsistencias";
            this.DataAsistencias.ReadOnly = true;
            this.DataAsistencias.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.DataAsistencias.RowHeadersVisible = false;
            this.DataAsistencias.RowHeadersWidth = 51;
            this.DataAsistencias.RowTemplate.Height = 25;
            this.DataAsistencias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataAsistencias.Size = new System.Drawing.Size(650, 380);
            this.DataAsistencias.TabIndex = 10;
            // 
            // FormAsistencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(750, 550);
            this.Controls.Add(this.DataAsistencias);
            this.Controls.Add(this.ComboEquipos);
            this.Controls.Add(this.PorDia);
            this.Controls.Add(this.PorMes);
            this.Controls.Add(this.dtAsistencia);
            this.Controls.Add(this.ButtonImportar);
            this.Controls.Add(this.IconExit);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "FormAsistencia";
            this.Text = "FormAsistencia";
            this.Load += new System.EventHandler(this.FormAsistencia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataAsistencias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton IconExit;
        private FontAwesome.Sharp.IconButton ButtonImportar;
        private System.Windows.Forms.DateTimePicker dtAsistencia;
        private System.Windows.Forms.RadioButton PorMes;
        private System.Windows.Forms.RadioButton PorDia;
        private System.Windows.Forms.ComboBox ComboEquipos;
        private System.Windows.Forms.DataGridView DataAsistencias;
    }
}