﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Services;
using SofiaNetCore.Services.Strategy;
using SofiaNetCore.Utility;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormAsistencia : Form
    {
        private readonly AppDbContext _db;
        private readonly IFtpServer _ftpServer;
        private readonly IUsuarioData _usuario;
        private bool estado = false;
        public FormAsistencia(AppDbContext appDb, IFtpServer ftpServer, IUsuarioData usuario)
        {
            InitializeComponent();
            _db = appDb;
            _ftpServer = ftpServer;
            _usuario = usuario;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void ButtonImportar_Click(object sender, EventArgs e)
        {
            try
            {
                using OpenFileDialog archive = new();
                archive.Title = "Importar Asistencia";
                archive.InitialDirectory = @"C:\\";
                archive.Filter = "Libro de Excel (*.xlsx)|*.xlsx|Excel 97-2003 (*.xls)|*.xls";
                archive.FilterIndex = 0;
                archive.RestoreDirectory = true;
                if (archive.ShowDialog() == DialogResult.OK)
                {
                    ButtonImportar.Enabled = false;
                    string NombreNormalizado = UFunciones.NormalizarAsistencia(PorMes.Checked, dtAsistencia.Value, ComboEquipos.Text);
                    var oExcel = new ExcelContext(new AsistenciaBiom());
                    await oExcel.CargarExcelAsync(archive.FileName, _db, _ftpServer, "", NombreNormalizado, _usuario.Usr_codigo ?? "ADMIN");
                    _ = await _db.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[Aprueba_Asistencia] {dtAsistencia.Value}, {PorMes.Checked}, {NombreNormalizado}");
                    ActualizaDatos();
                    MessageBox.Show("Archivo importado correctamente", "Excel importado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Ocurrio un problema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ButtonImportar.Enabled = true;
            }
        }
        private void FormAsistencia_Load(object sender, EventArgs e)
        {
            // DataGridView
            DataAsistencias.DataSource = null;
            // Carga de equipos
            ComboEquipos.DataSource = _db.RvEquipoAsistencia.AsNoTracking().ToList();
            ComboEquipos.DisplayMember = "descripcion";
            ComboEquipos.ValueMember = "IdAsist";
            ComboEquipos.SelectedIndex = 0;
            // Valores por defecto del DateTimePicker
            dtAsistencia.CustomFormat = PorMes.Checked ? " MM / yyyy" : " dd/MM/yyyy";
            dtAsistencia.Value = DateTime.Now;
            //ActualizarDatos();
            estado = true;
        }
        private void PorMes_CheckedChanged(object sender, EventArgs e)
        {
            dtAsistencia.CustomFormat = " MM / yyyy";
            if (PorMes.Checked)
            {
                ActualizaDatos();
            }
        }
        private void PorDia_CheckedChanged(object sender, EventArgs e)
        {
            dtAsistencia.CustomFormat = " dd/MM/yyyy";
            if (PorDia.Checked)
            {
                ActualizaDatos();
            }
        }
        private async void ActualizaDatos()
        {
            if (!estado)
            {
                return;
            }
            // Cargar DataGridView
            DataAsistencias.DataSource = null;
            // Variables
            int AsistId = Convert.ToInt32(ComboEquipos.SelectedValue!.ToString());
            if (AsistId == 0)
            {
                return;
            }
            string NombreNormalizado = UFunciones.NormalizarAsistencia(PorMes.Checked, dtAsistencia.Value, ComboEquipos.Text);
            if (PorMes.Checked)
            {
                DataAsistencias.DataSource = await _db.RoControlAsistencia.AsNoTracking().Where(m => m.FechaControl.Year == dtAsistencia.Value.Year && m.FechaControl.Month == dtAsistencia.Value.Month && m.Archivo == NombreNormalizado).ToListAsync();
            }
            else
            {
                DataAsistencias.DataSource = await _db.RoControlAsistencia.AsNoTracking().Where(m => m.FechaControl.Year == dtAsistencia.Value.Year && m.FechaControl.Month == dtAsistencia.Value.Month && m.FechaControl.Day == dtAsistencia.Value.Day && m.Archivo == NombreNormalizado).ToListAsync();
            }
        }

        private void ComboEquipos_SelectedValueChanged(object sender, EventArgs e)
        {
            ActualizaDatos();
        }

        private void dtAsistencia_ValueChanged(object sender, EventArgs e)
        {
            ActualizaDatos();
        }
    }
}
