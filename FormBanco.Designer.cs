﻿namespace SofiaNetCore
{
    partial class FormBanco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ButtonImportar = new FontAwesome.Sharp.IconButton();
            this.ComboBancos = new System.Windows.Forms.ComboBox();
            this.DataExtractos = new System.Windows.Forms.DataGridView();
            this.IconExit = new FontAwesome.Sharp.IconButton();
            this.PorDia = new System.Windows.Forms.RadioButton();
            this.PorMes = new System.Windows.Forms.RadioButton();
            this.dtExtracto = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.DataExtractos)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonImportar
            // 
            this.ButtonImportar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ButtonImportar.FlatAppearance.BorderSize = 0;
            this.ButtonImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonImportar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ButtonImportar.ForeColor = System.Drawing.Color.White;
            this.ButtonImportar.IconChar = FontAwesome.Sharp.IconChar.FileUpload;
            this.ButtonImportar.IconColor = System.Drawing.Color.White;
            this.ButtonImportar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonImportar.IconSize = 30;
            this.ButtonImportar.Location = new System.Drawing.Point(549, 60);
            this.ButtonImportar.Name = "ButtonImportar";
            this.ButtonImportar.Size = new System.Drawing.Size(140, 50);
            this.ButtonImportar.TabIndex = 2;
            this.ButtonImportar.Text = "IMPORTAR";
            this.ButtonImportar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonImportar.UseVisualStyleBackColor = false;
            this.ButtonImportar.Click += new System.EventHandler(this.ButtonImportar_Click);
            // 
            // ComboBancos
            // 
            this.ComboBancos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ComboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBancos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboBancos.ForeColor = System.Drawing.Color.White;
            this.ComboBancos.FormattingEnabled = true;
            this.ComboBancos.Location = new System.Drawing.Point(285, 70);
            this.ComboBancos.Margin = new System.Windows.Forms.Padding(0);
            this.ComboBancos.Name = "ComboBancos";
            this.ComboBancos.Size = new System.Drawing.Size(210, 31);
            this.ComboBancos.TabIndex = 1;
            this.ComboBancos.SelectedValueChanged += new System.EventHandler(this.ComboBancos_SelectedValueChanged);
            // 
            // DataExtractos
            // 
            this.DataExtractos.AllowUserToAddRows = false;
            this.DataExtractos.AllowUserToDeleteRows = false;
            this.DataExtractos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.DataExtractos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataExtractos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataExtractos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataExtractos.ColumnHeadersHeight = 30;
            this.DataExtractos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(42)))), ((int)(((byte)(83)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataExtractos.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataExtractos.EnableHeadersVisualStyles = false;
            this.DataExtractos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(31)))), ((int)(((byte)(32)))));
            this.DataExtractos.Location = new System.Drawing.Point(50, 137);
            this.DataExtractos.Name = "DataExtractos";
            this.DataExtractos.ReadOnly = true;
            this.DataExtractos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.DataExtractos.RowHeadersVisible = false;
            this.DataExtractos.RowHeadersWidth = 51;
            this.DataExtractos.RowTemplate.Height = 25;
            this.DataExtractos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataExtractos.Size = new System.Drawing.Size(650, 380);
            this.DataExtractos.TabIndex = 4;
            // 
            // IconExit
            // 
            this.IconExit.FlatAppearance.BorderSize = 0;
            this.IconExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconExit.IconChar = FontAwesome.Sharp.IconChar.Multiply;
            this.IconExit.IconColor = System.Drawing.Color.White;
            this.IconExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconExit.IconSize = 30;
            this.IconExit.Location = new System.Drawing.Point(708, 13);
            this.IconExit.Name = "IconExit";
            this.IconExit.Size = new System.Drawing.Size(30, 33);
            this.IconExit.TabIndex = 5;
            this.IconExit.UseVisualStyleBackColor = true;
            this.IconExit.Click += new System.EventHandler(this.IconExit_Click);
            // 
            // PorDia
            // 
            this.PorDia.AutoSize = true;
            this.PorDia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PorDia.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PorDia.Location = new System.Drawing.Point(181, 26);
            this.PorDia.Name = "PorDia";
            this.PorDia.Size = new System.Drawing.Size(134, 27);
            this.PorDia.TabIndex = 8;
            this.PorDia.Text = "Carga por día";
            this.PorDia.UseVisualStyleBackColor = true;
            this.PorDia.CheckedChanged += new System.EventHandler(this.PorDia_CheckedChanged);
            // 
            // PorMes
            // 
            this.PorMes.AutoSize = true;
            this.PorMes.Checked = true;
            this.PorMes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PorMes.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PorMes.Location = new System.Drawing.Point(32, 26);
            this.PorMes.Name = "PorMes";
            this.PorMes.Size = new System.Drawing.Size(142, 27);
            this.PorMes.TabIndex = 7;
            this.PorMes.TabStop = true;
            this.PorMes.Text = "Carga por mes";
            this.PorMes.UseVisualStyleBackColor = true;
            this.PorMes.CheckedChanged += new System.EventHandler(this.PorMes_CheckedChanged);
            // 
            // dtExtracto
            // 
            this.dtExtracto.CalendarFont = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtExtracto.CustomFormat = "";
            this.dtExtracto.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtExtracto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExtracto.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dtExtracto.Location = new System.Drawing.Point(86, 71);
            this.dtExtracto.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtExtracto.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtExtracto.Name = "dtExtracto";
            this.dtExtracto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtExtracto.Size = new System.Drawing.Size(145, 30);
            this.dtExtracto.TabIndex = 9;
            this.dtExtracto.Value = new System.DateTime(2011, 12, 26, 15, 32, 0, 0);
            this.dtExtracto.ValueChanged += new System.EventHandler(this.dtExtracto_ValueChanged);
            // 
            // FormBanco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(750, 550);
            this.Controls.Add(this.dtExtracto);
            this.Controls.Add(this.PorDia);
            this.Controls.Add(this.PorMes);
            this.Controls.Add(this.IconExit);
            this.Controls.Add(this.DataExtractos);
            this.Controls.Add(this.ComboBancos);
            this.Controls.Add(this.ButtonImportar);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBanco";
            this.Text = "FormExcel";
            this.Load += new System.EventHandler(this.FormBanco_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataExtractos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FontAwesome.Sharp.IconButton ButtonImportar;
        private System.Windows.Forms.ComboBox ComboBancos;
        private System.Windows.Forms.DataGridView DataExtractos;
        private FontAwesome.Sharp.IconButton IconExit;
        private System.Windows.Forms.RadioButton PorDia;
        private System.Windows.Forms.RadioButton PorMes;
        private System.Windows.Forms.DateTimePicker dtExtracto;
    }
}