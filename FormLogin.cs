using Microsoft.Extensions.DependencyInjection;
using SofiaNetCore.Services;
using System;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormLogin : Form
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IUsuarioData _user;
        public FormLogin(IServiceProvider serviceProv, IUsuarioData user)
        {
            InitializeComponent();
            _serviceProvider = serviceProv;
            _user = user;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void ButtonLogin_Click(object sender, EventArgs e)
        {
            try
            {
                ButtonLogin.Enabled = false;
                bool auth = await _user.Login(textBoxUser.Text, textBoxPass.Text);
                if (auth)
                {
                    FormMain frmMain = _serviceProvider.GetRequiredService<FormMain>();
                    frmMain.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Usuario o contraseņa incorrecta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //textBoxUser.Clear();
                    textBoxPass.Clear();
                    textBoxUser.Focus();
                }
            }
            finally
            {
                ButtonLogin.Enabled = true;
            }
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            textBoxUser.Focus();
        }

        private void textBoxUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);
        }
    }
}