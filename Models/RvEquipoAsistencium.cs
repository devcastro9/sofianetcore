﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Keyless]
    public partial class RvEquipoAsistencium
    {
        [Column("id_asist")]
        public int IdAsist { get; set; }
        [Column("descripcion")]
        [StringLength(60)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
    }
}
