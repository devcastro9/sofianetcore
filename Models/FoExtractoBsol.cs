﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SofiaNetCore.Models
{
    [Table("fo_extracto_BSOL")]
    public partial class FoExtractoBsol
    {
        [Key]
        [Column("correlativo")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Correlativo { get; set; }
        [Column("fecha", TypeName = "date")]
        public DateTime? Fecha { get; set; }
        [Column("hora")]
        [Precision(0)]
        public TimeSpan? Hora { get; set; }
        [Column("nro_transaccion")]
        [StringLength(30)]
        [Unicode(false)]
        public string? NroTransaccion { get; set; }
        [Column("descripcion")]
        [StringLength(200)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
        [Column("nota_glosa")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NotaGlosa { get; set; }
        [Column("monto", TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [Column("saldo", TypeName = "decimal(18, 2)")]
        public decimal? Saldo { get; set; }
        [Column("cuenta")]
        [StringLength(40)]
        [Unicode(false)]
        public string? Cuenta { get; set; }
        [Column("estado_conciliado")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoConciliado { get; set; }
        [Column("usuario")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Usuario { get; set; }
        [Column("nombre_archivo")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }
        [Column("fecha_subido", TypeName = "datetime")]
        public DateTime? FechaSubido { get; set; }
    }
}
