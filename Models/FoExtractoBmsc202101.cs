﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("fo_extracto_BMSC_202101")]
    public partial class FoExtractoBmsc202101
    {
        [Key]
        [Column("correlativo")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Correlativo { get; set; }
        [Column("fecha_transaccion", TypeName = "date")]
        public DateTime? FechaTransaccion { get; set; }
        [Column("hora_transaccion")]
        public TimeSpan? HoraTransaccion { get; set; }
        [Column("cod_bancarizacion")]
        [StringLength(40)]
        [Unicode(false)]
        public string? CodBancarizacion { get; set; }
        [Column("nro_cheque")]
        [StringLength(15)]
        [Unicode(false)]
        public string? NroCheque { get; set; }
        [Column("plantilla")]
        [StringLength(40)]
        [Unicode(false)]
        public string? Plantilla { get; set; }
        [Column("cod_cliente")]
        [StringLength(20)]
        [Unicode(false)]
        public string? CodCliente { get; set; }
        [Column("id_depositante")]
        [StringLength(15)]
        [Unicode(false)]
        public string? IdDepositante { get; set; }
        [Column("nombre_depositante")]
        [StringLength(150)]
        [Unicode(false)]
        public string? NombreDepositante { get; set; }
        [Column("tipo_transaccion")]
        [StringLength(10)]
        [Unicode(false)]
        public string? TipoTransaccion { get; set; }
        [Column("descripcion")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
        [Column("oficina")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Oficina { get; set; }
        [Column("banco")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Banco { get; set; }
        [Column("tipo_deposito")]
        [StringLength(10)]
        [Unicode(false)]
        public string? TipoDeposito { get; set; }
        [Column("nombre_destinatario")]
        [StringLength(100)]
        [Unicode(false)]
        public string? NombreDestinatario { get; set; }
        [Column("glosa")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Glosa { get; set; }
        [Column("originador")]
        [StringLength(100)]
        [Unicode(false)]
        public string? Originador { get; set; }
        [Column("originador_ACH")]
        [StringLength(100)]
        [Unicode(false)]
        public string? OriginadorAch { get; set; }
        [Column("ciudad_origen")]
        [StringLength(10)]
        [Unicode(false)]
        public string? CiudadOrigen { get; set; }
        [Column("debito", TypeName = "decimal(18, 2)")]
        public decimal? Debito { get; set; }
        [Column("credito", TypeName = "decimal(18, 2)")]
        public decimal? Credito { get; set; }
        [Column("saldo", TypeName = "decimal(18, 2)")]
        public decimal? Saldo { get; set; }
        [StringLength(40)]
        [Unicode(false)]
        public string? Cuenta { get; set; }
        [Column("estado_conciliado")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoConciliado { get; set; }
        [Column("usuario")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Usuario { get; set; }
        [Column("nombre_archivo")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }
        [Column("fecha_subido", TypeName = "datetime")]
        public DateTime? FechaSubido { get; set; }
    }
}
