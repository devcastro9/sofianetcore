﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Keyless]
    public partial class FvExtractoCuenta
    {
        [Column("cuenta")]
        [StringLength(40)]
        [Unicode(false)]
        public string Cuenta { get; set; } = null!;
        [Column("banco")]
        [StringLength(10)]
        [Unicode(false)]
        public string? Banco { get; set; }
        [StringLength(60)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
    }
}
