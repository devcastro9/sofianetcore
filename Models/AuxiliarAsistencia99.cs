﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("auxiliar_asistencia_99")]
    public partial class AuxiliarAsistencia99
    {
        [Key]
        [Column("Id_AuxAsis")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdAuxAsis { get; set; }
        public int? Nro { get; set; }
        [Column("AC_No")]
        public int? AcNo { get; set; }
        [Column("Cedula_No")]
        [StringLength(20)]
        [Unicode(false)]
        public string? CedulaNo { get; set; }
        [StringLength(300)]
        [Unicode(false)]
        public string? Nombre { get; set; }
        [Column("Auto_asigna")]
        [StringLength(15)]
        [Unicode(false)]
        public string? AutoAsigna { get; set; }
        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? Horario { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraEnt { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraSal { get; set; }
        [Column("Marc_Ent", TypeName = "time(0)")]
        public TimeSpan? MarcEnt { get; set; }
        [Column("Marc_Sal", TypeName = "time(0)")]
        public TimeSpan? MarcSal { get; set; }
        [Column(TypeName = "decimal(12, 2)")]
        public decimal? Normal { get; set; }
        [Column(TypeName = "decimal(12, 2)")]
        public decimal? TiemReal { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? Tardanza { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? SalioTempr { get; set; }
        public bool? Falta { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? HoraExtra { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? WorkTime { get; set; }
        [StringLength(30)]
        [Unicode(false)]
        public string? Excepcion { get; set; }
        [Column("Debe_C_In")]
        public bool? DebeCIn { get; set; }
        [Column("Debe_C_Sal")]
        public bool? DebeCSal { get; set; }
        [StringLength(300)]
        [Unicode(false)]
        public string? Depto { get; set; }
        [Column("NDays", TypeName = "decimal(12, 2)")]
        public decimal? Ndays { get; set; }
        [StringLength(20)]
        [Unicode(false)]
        public string? FinSemana { get; set; }
        [StringLength(20)]
        [Unicode(false)]
        public string? Feriado { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? TiemAsist { get; set; }
        [Column("NDiasOT", TypeName = "decimal(12, 2)")]
        public decimal? NdiasOt { get; set; }
        [Column("FinSemanaOT")]
        [StringLength(20)]
        [Unicode(false)]
        public string? FinSemanaOt { get; set; }
        [Column("FeriadoOT")]
        [StringLength(20)]
        [Unicode(false)]
        public string? FeriadoOt { get; set; }
        [Column("nombreArchivo")]
        [StringLength(300)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }
        [Column("sLastUpdate_id")]
        [StringLength(20)]
        [Unicode(false)]
        public string? SLastUpdateId { get; set; }
        [Column("dtLastUpdate_dt", TypeName = "datetime")]
        public DateTime? DtLastUpdateDt { get; set; }
    }
}
