﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("fo_extracto_BGA_99")]
    public partial class FoExtractoBga99
    {
        [Key]
        [Column("correlativo")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Correlativo { get; set; }


        [Column("fecha", TypeName = "date")]
        public DateTime? Fecha { get; set; }
        [Column("hora")]
        public TimeSpan? Hora { get; set; }
        [Column("oficina")]
        [StringLength(30)]
        [Unicode(false)]
        public string? Oficina { get; set; }
        [Column("nro_transaccion")]
        [StringLength(40)]
        [Unicode(false)]
        public string? NroTransaccion { get; set; }
        [Column("descripcion")]
        [StringLength(200)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
        [Column("debito", TypeName = "decimal(18, 2)")]
        public decimal? Debito { get; set; }
        [Column("credito", TypeName = "decimal(18, 2)")]
        public decimal? Credito { get; set; }
        [Column("saldo", TypeName = "decimal(18, 2)")]
        public decimal? Saldo { get; set; }

        [Column("cuenta")]
        [StringLength(40)]
        [Unicode(false)]
        public string? Cuenta { get; set; }
        [Column("estado_conciliado")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoConciliado { get; set; }
        [Column("usuario")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Usuario { get; set; }
        [Column("nombre_archivo")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }
        [Column("fecha_subido", TypeName = "datetime")]
        public DateTime? FechaSubido { get; set; }
    }
}
