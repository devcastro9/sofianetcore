﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("fo_extracto_BCP_99")]
    public partial class FoExtractoBcp99
    {
        [Key]
        [Column("correlativo")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Correlativo { get; set; }

        [Column("fecha", TypeName = "date")]
        public DateTime? Fecha { get; set; }

        [Column("hora")]
        [Precision(0)]
        public TimeSpan? Hora { get; set; }

        [Column("glosa")]
        [StringLength(30)]
        [Unicode(false)]
        public string? Glosa { get; set; }

        [Column("importe", TypeName = "decimal(18, 4)")]
        public decimal? Importe { get; set; }

        [Column("descripcion")]
        [StringLength(180)]
        [Unicode(false)]
        public string? Descripcion { get; set; }

        [Column("canal")]
        [StringLength(30)]
        [Unicode(false)]
        public string? Canal { get; set; }

        [Column("sucursal_agencia")]
        [StringLength(30)]
        [Unicode(false)]
        public string? SucursalAgencia { get; set; }

        [Column("nro_operacion")]
        [StringLength(20)]
        [Unicode(false)]
        public string? NroOperacion { get; set; }

        [Column("cuenta")]
        [StringLength(40)]
        [Unicode(false)]
        public string? Cuenta { get; set; }

        [Column("estado_conciliado")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoConciliado { get; set; }

        [Column("usuario")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Usuario { get; set; }

        [Column("nombre_archivo")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }

        [Column("fecha_subido", TypeName = "datetime")]
        public DateTime? FechaSubido { get; set; }
    }
}
