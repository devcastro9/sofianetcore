﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("ro_ControlAsistencia")]
    public partial class RoControlAsistencium
    {
        [Key]
        [Column("id_controlasistencia")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long IdControlasistencia { get; set; }
        [Column("beneficiario_codigo")]
        [StringLength(20)]
        [Unicode(false)]
        public string BeneficiarioCodigo { get; set; } = null!;
        public int Correl { get; set; }
        [Column("Fecha_control", TypeName = "date")]
        public DateTime FechaControl { get; set; }
        [Column("ges_gestion")]
        [StringLength(4)]
        [Unicode(false)]
        public string? GesGestion { get; set; }
        [Column("Mes_control")]
        [StringLength(12)]
        [Unicode(false)]
        public string? MesControl { get; set; }
        [Column("Dia_control")]
        [StringLength(10)]
        [Unicode(false)]
        public string? DiaControl { get; set; }
        [Column("turno")]
        [StringLength(2)]
        [Unicode(false)]
        public string? Turno { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? Hora1 { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? Hora2 { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraUno { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraDos { get; set; }
        [StringLength(2)]
        [Unicode(false)]
        public string? Atraso { get; set; }
        [StringLength(3)]
        [Unicode(false)]
        public string? Falta { get; set; }
        public int? TotalMin1 { get; set; }
        public int? AtrasoMin1 { get; set; }
        [Column("turno2")]
        [StringLength(2)]
        [Unicode(false)]
        public string? Turno2 { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? Hora3 { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? Hora4 { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraTres { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraCuatro { get; set; }
        [StringLength(2)]
        [Unicode(false)]
        public string? AtrasoI { get; set; }
        [StringLength(3)]
        [Unicode(false)]
        public string? Falta2 { get; set; }
        public int? TotalMin2 { get; set; }
        public int? AtrasoMin2 { get; set; }
        [StringLength(300)]
        [Unicode(false)]
        public string? Archivo { get; set; }
        [Column("Min_Tolerancia")]
        public int? MinTolerancia { get; set; }
        [Column("horas_extras")]
        [StringLength(2)]
        [Unicode(false)]
        public string? HorasExtras { get; set; }
        [Column("minutos_extras")]
        [StringLength(2)]
        [Unicode(false)]
        public string? MinutosExtras { get; set; }
        public int? Item { get; set; }
        [Column("Ing_Sal")]
        [StringLength(1)]
        [Unicode(false)]
        public string? IngSal { get; set; }
        [Column("estado_registro")]
        [StringLength(2)]
        [Unicode(false)]
        public string? EstadoRegistro { get; set; }
        [Column("usr_usuario")]
        [StringLength(15)]
        [Unicode(false)]
        public string? UsrUsuario { get; set; }
        [Column("fecha_registro", TypeName = "datetime")]
        public DateTime? FechaRegistro { get; set; }
        [Column("hara_registro")]
        [StringLength(8)]
        [Unicode(false)]
        public string? HaraRegistro { get; set; }
        [Column("Correl_ac")]
        public int? CorrelAc { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string? Nombre { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? Autoasigna { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? TipoHorario { get; set; }
        [Column(TypeName = "decimal(12, 2)")]
        public decimal? Normal { get; set; }
        [Column(TypeName = "decimal(12, 2)")]
        public decimal? TiemReal { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? SalioTempr { get; set; }
        public bool? EsFalta { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? HoraExtra { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? HoraExtra2 { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? WorkTime { get; set; }
        [StringLength(150)]
        [Unicode(false)]
        public string? Excepcion { get; set; }
        [Column("Debe_C_In")]
        public bool? DebeCIn { get; set; }
        [Column("Debe_C_Sal")]
        public bool? DebeCSal { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? Depto { get; set; }
        [Column("NDays", TypeName = "decimal(12, 2)")]
        public decimal? Ndays { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? FinSemana { get; set; }
        [StringLength(15)]
        [Unicode(false)]
        public string? Feriado { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? TiemAsist { get; set; }
        [Column("NDiasOT", TypeName = "decimal(12, 2)")]
        public decimal? NdiasOt { get; set; }
        [Column("FinSemanaOT")]
        [StringLength(15)]
        [Unicode(false)]
        public string? FinSemanaOt { get; set; }
        [Column("FeriadoOT")]
        [StringLength(15)]
        [Unicode(false)]
        public string? FeriadoOt { get; set; }
        [Column(TypeName = "time(0)")]
        public TimeSpan? Tardanza { get; set; }
        [Column("Id_AuxAsis")]
        public int? IdAuxAsis { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? TardanzaCadena { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string? TiempoTrabajoCadena { get; set; }
        [Column("sLastUpdate_id")]
        [StringLength(20)]
        [Unicode(false)]
        public string? SLastUpdateId { get; set; }
        [Column("dtLastUpdate_dt", TypeName = "datetime")]
        public DateTime? DtLastUpdateDt { get; set; }
    }
}
