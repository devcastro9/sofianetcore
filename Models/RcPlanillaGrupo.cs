﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("rc_planilla_grupo")]
    public partial class RcPlanillaGrupo
    {
        [Key]
        [Column("planilla_codigo")]
        [StringLength(3)]
        [Unicode(false)]
        public string PlanillaCodigo { get; set; } = null!;
        [Column("planilla_descripcion")]
        [StringLength(150)]
        [Unicode(false)]
        public string? PlanillaDescripcion { get; set; }
        [Column("depto_codigo")]
        [StringLength(2)]
        [Unicode(false)]
        public string? DeptoCodigo { get; set; }
        [Column("registro_patronal")]
        [StringLength(50)]
        [Unicode(false)]
        public string? RegistroPatronal { get; set; }
        [Column("estado_codigo")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoCodigo { get; set; }
        [Column("usr_codigo")]
        [StringLength(15)]
        [Unicode(false)]
        public string? UsrCodigo { get; set; }
        [Column("Fecha_Registro", TypeName = "smalldatetime")]
        public DateTime? FechaRegistro { get; set; }
    }
}
