﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofiaNetCore.Models
{
    [Table("fo_extracto_BUNION_99")]
    public partial class FoExtractoBunion99
    {
        [Key]
        [Column("correlativo")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Correlativo { get; set; }
        [Column("fecha", TypeName = "date")]
        public DateTime? Fecha { get; set; }
        [Column("agencia")]
        [StringLength(5)]
        [Unicode(false)]
        public string? Agencia { get; set; }
        [Column("descripcion")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
        [Column("nroDocumento")]
        [StringLength(12)]
        [Unicode(false)]
        public string? NroDocumento { get; set; }
        [Column("monto", TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [Column("saldo", TypeName = "decimal(18, 2)")]
        public decimal? Saldo { get; set; }
        [Column("cuenta")]
        [StringLength(40)]
        [Unicode(false)]
        public string? Cuenta { get; set; }
        [Column("estado_conciliado")]
        [StringLength(3)]
        [Unicode(false)]
        public string? EstadoConciliado { get; set; }
        [Column("usuario")]
        [StringLength(20)]
        [Unicode(false)]
        public string? Usuario { get; set; }
        [Column("nombre_archivo")]
        [StringLength(200)]
        [Unicode(false)]
        public string? NombreArchivo { get; set; }
        [Column("fecha_subido", TypeName = "datetime")]
        public DateTime? FechaSubido { get; set; }
    }
}
