﻿using ExcelDataReader;
using SofiaNetCore.Data;
using SofiaNetCore.Models;
using SofiaNetCore.Utility;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SofiaNetCore.Services.Strategy
{
    public class AsistenciaBiom : IExcelStrategy
    {
        private readonly ExcelDataSetConfiguration config = new()
        {
            UseColumnDataType = false,
            FilterSheet = (tableReader, sheetIndex) => sheetIndex == 0,
            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            {
                UseHeaderRow = true,
                FilterRow = (rowHeader) => rowHeader != null,
                FilterColumn = (rowHeader, columnIndex) => rowHeader[columnIndex] != null
            }
        };
        public async Task UploadAsync(string RutaExcel, AppDbContext Context, IFtpServer ftpServer, string Identificador, string NombreNormalizado, string NombreUsuario)
        {
            // Variables
            int numeroLote = 20;
            int contador = 1;
            DateTime FechaActual = DateTime.Now;
            // Lectura de Excel
            DataSet result = new();
            // Respaldar
            string CarpetaRespaldo = Path.Combine(AppContext.BaseDirectory, "Asistencia");
            string PathRespaldo = Path.Combine(CarpetaRespaldo, NombreNormalizado + Path.GetExtension(RutaExcel));
            _ = Directory.CreateDirectory(CarpetaRespaldo);
            File.Copy(RutaExcel, PathRespaldo, true);
            // Leer Excel
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            using (FileStream stream = File.Open(PathRespaldo, FileMode.Open, FileAccess.Read))
            {
                // Posee autodeteccion de versiones de Excel:
                // - Binary Excel:  *.xls   (SpreadSheetLight no soporta)
                // - OpenXml Excel: *.xlsx  (SpreadSheetLight soporta)
                using IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);
                result = reader.AsDataSet(config);
            }
            // Respaldo en FTP
            ftpServer.Respaldar(PathRespaldo, NombreUsuario, "Asistencia");
            foreach (DataRow row in result.Tables[0].Rows)
            {
                // Model
                AuxiliarAsistencia99 model = new()
                {
                    Nro = Convert.ToInt32(row[0].ToString()),
                    AcNo = Convert.ToInt32(row[1].ToString()),
                    CedulaNo = UFunciones.Recort(row[2].ToString()),
                    Nombre = UFunciones.Recort(row[3].ToString()),
                    AutoAsigna = UFunciones.Recort(row[4].ToString()),
                    Fecha = UFunciones.ToDTime(row[5].ToString()),
                    Horario = UFunciones.Recort(row[6].ToString()),
                    HoraEnt = UFunciones.ToTime(row[7].ToString()),
                    HoraSal = UFunciones.ToTime(row[8].ToString()),
                    MarcEnt = UFunciones.ToTime(row[9].ToString()),
                    MarcSal = UFunciones.ToTime(row[10].ToString()),
                    Normal = UFunciones.RecortDec(row[11].ToString()),
                    TiemReal = UFunciones.RecortDec(row[12].ToString()),
                    Tardanza = UFunciones.ToTime(row[13].ToString()),
                    SalioTempr = UFunciones.ToTime(row[14].ToString()),
                    Falta = UFunciones.ToBoolean(row[15].ToString()),
                    HoraExtra = UFunciones.Recort(row[16].ToString()),
                    WorkTime = UFunciones.ToTime(row[17].ToString()),
                    Excepcion = UFunciones.Recort(row[18].ToString()),
                    DebeCIn = UFunciones.ToBoolean(row[19].ToString()),
                    DebeCSal = UFunciones.ToBoolean(row[20].ToString()),
                    Depto = UFunciones.Recort(row[21].ToString()),
                    Ndays = UFunciones.RecortDec(row[22].ToString()),
                    FinSemana = UFunciones.Recort(row[23].ToString()),
                    Feriado = UFunciones.Recort(row[24].ToString()),
                    TiemAsist = UFunciones.ToTime(row[25].ToString()),
                    NdiasOt = UFunciones.RecortDec(row[26].ToString()),
                    FinSemanaOt = UFunciones.Recort(row[27].ToString()),
                    FeriadoOt = UFunciones.Recort(row[28].ToString()),
                    NombreArchivo = NombreNormalizado,
                    SLastUpdateId = NombreUsuario,
                    DtLastUpdateDt = FechaActual
                };
                await Context.AuxiliarAsistencia99s.AddAsync(model);
                // Guardado de cambios por lote
                if (contador % numeroLote == 0)
                {
                    try
                    {
                        _ = await Context.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        // DbUpdateConcurrencyException
                        throw;
                    }
                }
                contador++;
            }
            if (contador % numeroLote == 0)
            {
                try
                {
                    _ = await Context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    // DbUpdateConcurrencyException
                    throw;
                }
            }
            // Cargado a tabla oficial
            //_ = await Context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[Aprueba_Asistencia] {Identificador}, {1}, {NombreNormalizado}");
        }
    }
}
