﻿using ExcelDataReader;
using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Models;
using SofiaNetCore.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SofiaNetCore.Services.Strategy
{
    public class ExtractoBUNION : IExcelStrategy
    {
        private static readonly List<int> iColumns = new() { 1, 3, 7, 20, 25, 29 };
        private readonly ExcelDataSetConfiguration config = new()
        {
            UseColumnDataType = true,
            FilterSheet = (tableReader, sheetIndex) => sheetIndex == 0,
            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            {
                EmptyColumnNamePrefix = "Col",
                FilterRow = (rowReader) => rowReader[1] != null && UFunciones.VerificarFormato(rowReader[1].ToString()),
                FilterColumn = (rowReader, columnIndex) => iColumns.Contains(columnIndex)
            }
        };
        private readonly ExcelDataSetConfiguration cuenta_config = new()
        {
            UseColumnDataType = false,
            FilterSheet = (tableReader, sheetIndex) => sheetIndex == 0,
            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            {
                EmptyColumnNamePrefix = "Cuenta",
                FilterRow = (rowReader) => rowReader.Depth == 7,
                FilterColumn = (rowReader, columnIndex) => columnIndex == 4
            }
        };
        public async Task UploadAsync(string RutaExcel, AppDbContext Context, IFtpServer ftpServer, string Identificador, string NombreNormalizado, string NombreUsuario)
        {
            // Variables
            int numeroLote = 20;
            int contador = 1;
            DateTime FechaActual = DateTime.Now;
            // Lectura de Excel
            DataSet result = new();
            string? cuenta;
            // Respaldar
            string CarpetaRespaldo = Path.Combine(AppContext.BaseDirectory, "Extracto");
            string PathRespaldo = Path.Combine(CarpetaRespaldo, NombreNormalizado + Path.GetExtension(RutaExcel));
            _ = Directory.CreateDirectory(CarpetaRespaldo);
            File.Copy(RutaExcel, PathRespaldo, true);
            // Leer Excel
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            using (FileStream stream = File.Open(PathRespaldo, FileMode.Open, FileAccess.Read))
            {
                // Posee autodeteccion de versiones de Excel:
                // - Binary Excel:  *.xls   (SpreadSheetLight no soporta)
                // - OpenXml Excel: *.xlsx  (SpreadSheetLight soporta)
                using IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);
                cuenta = reader.AsDataSet(cuenta_config).Tables[0].Rows[0][0].ToString();
                result = reader.AsDataSet(config);
            }
            // Validacion de Cuenta Bancaria
            if (cuenta == null)
            {
                throw new CuentaException($"No se puede determinar la cuenta desde el archivo Excel");
            }
            if (Identificador != cuenta)
            {
                throw new CuentaException($"La cuenta del archivo {cuenta} y la cuenta seleccionada {Identificador} no coinciden");
            }
            // Respaldo en FTP
            ftpServer.Respaldar(PathRespaldo, NombreUsuario, "Extractos");
            // Cargado a Sql Server
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                FoExtractoBunion99 model = new()
                {
                    Fecha = UFunciones.ToDTime(dr[0].ToString()),
                    Agencia = UFunciones.Recort(dr[1].ToString()),
                    Descripcion = UFunciones.Recort(dr[2].ToString()),
                    NroDocumento = UFunciones.Recort(dr[3].ToString()),
                    Monto = UFunciones.RecortDec(dr[4].ToString()),
                    Saldo = UFunciones.RecortDec(dr[5].ToString()),
                    Cuenta = Identificador,
                    EstadoConciliado = "REG",
                    Usuario = NombreUsuario,
                    NombreArchivo = NombreNormalizado,
                    FechaSubido = FechaActual
                };
                await Context.FoExtractoBunion99s.AddAsync(model);
                // Guardado de cambios por lote
                if (contador % numeroLote == 0)
                {
                    try
                    {
                        _ = await Context.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        // DbUpdateConcurrencyException
                        throw;
                    }
                }
                contador++;
            }
            try
            {
                await Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
            // Cargado a tabla oficial
            _ = await Context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[Aprueba_BUNION]");
        }
    }
}
