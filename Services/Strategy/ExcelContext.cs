﻿using SofiaNetCore.Data;
using System.Threading.Tasks;

namespace SofiaNetCore.Services.Strategy
{
    public class ExcelContext
    {
        private readonly IExcelStrategy strategy;
        public ExcelContext(IExcelStrategy iStrategy)
        {
            strategy = iStrategy;
        }
        public async Task CargarExcelAsync(string RutaExcel, AppDbContext Context, IFtpServer ftpServer, string Identificador, string NombreNormalizado, string NombreUsuario)
        {
            await strategy.UploadAsync(RutaExcel, Context, ftpServer, Identificador, NombreNormalizado, NombreUsuario);
        }
    }
}
