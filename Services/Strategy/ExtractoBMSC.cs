﻿using ExcelDataReader;
using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Models;
using SofiaNetCore.Utility;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SofiaNetCore.Services.Strategy
{
    public class ExtractoBMSC : IExcelStrategy
    {
        private readonly ExcelDataSetConfiguration config = new()
        {
            UseColumnDataType = false,
            FilterSheet = (tableReader, sheetIndex) => sheetIndex == 0,
            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            {
                EmptyColumnNamePrefix = "Col",
                FilterRow = (rowReader) => rowReader[0] != null && UFunciones.VerificarFormato(rowReader[0].ToString()),
                FilterColumn = (rowReader, columnIndex) => columnIndex >= 0 && columnIndex <= 20
            }
        };
        private readonly ExcelDataSetConfiguration cuenta_config = new()
        {
            UseColumnDataType = false,
            FilterSheet = (tableReader, sheetIndex) => sheetIndex == 0,
            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
            {
                EmptyColumnNamePrefix = "Cuenta",
                FilterRow = (rowReader) => rowReader.Depth == 6,
                FilterColumn = (rowReader, columnIndex) => columnIndex == 5
            }
        };
        public async Task UploadAsync(string RutaExcel, AppDbContext Context, IFtpServer ftpServer, string Identificador, string NombreNormalizado, string NombreUsuario)
        {
            // Variables
            int numeroLote = 20;
            int contador = 1;
            DateTime FechaActual = DateTime.Now;
            // Lectura de Excel
            DataSet result = new();
            string? cuenta;
            // Respaldar
            string CarpetaRespaldo = Path.Combine(AppContext.BaseDirectory, "Extracto");
            string PathRespaldo = Path.Combine(CarpetaRespaldo, NombreNormalizado + Path.GetExtension(RutaExcel));
            _ = Directory.CreateDirectory(CarpetaRespaldo);
            File.Copy(RutaExcel, PathRespaldo, true);
            // Leer Excel
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            using (FileStream stream = File.Open(PathRespaldo, FileMode.Open, FileAccess.Read))
            {
                // Posee autodeteccion de versiones de Excel:
                // - Binary Excel:  *.xls   (SpreadSheetLight no soporta)
                // - OpenXml Excel: *.xlsx  (SpreadSheetLight soporta)
                using IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);
                cuenta = reader.AsDataSet(cuenta_config).Tables[0].Rows[0][0].ToString();
                result = reader.AsDataSet(config);
            }
            // Validacion de Cuenta Bancaria
            if (cuenta == null)
            {
                throw new CuentaException($"No se puede determinar la cuenta desde el archivo Excel");
            }
            if (Identificador != cuenta)
            {
                throw new CuentaException($"La cuenta del archivo {cuenta} y la cuenta seleccionada {Identificador} no coinciden");
            }
            // Respaldo en FTP
            ftpServer.Respaldar(PathRespaldo, NombreUsuario, "Extractos");
            // Cargado a Sql Server
            foreach (DataRow dr1 in result.Tables[0].Rows)
            {
                FoExtractoBmsc99 model = new()
                {
                    FechaTransaccion = UFunciones.ToDTime(dr1[0].ToString()),
                    HoraTransaccion = UFunciones.ToTime(dr1[1].ToString()),
                    CodBancarizacion = UFunciones.Recort(dr1[2].ToString()),
                    NroCheque = UFunciones.Recort(dr1[3].ToString()),
                    Plantilla = UFunciones.Recort(dr1[4].ToString()),
                    CodCliente = UFunciones.Recort(dr1[5].ToString()),
                    IdDepositante = UFunciones.Recort(dr1[6].ToString()),
                    NombreDepositante = UFunciones.Recort(dr1[7].ToString()),
                    TipoTransaccion = UFunciones.Recort(dr1[8].ToString()),
                    Descripcion = UFunciones.Recort(dr1[9].ToString()),
                    Oficina = UFunciones.Recort(dr1[10].ToString()),
                    Banco = UFunciones.Recort(dr1[11].ToString()),
                    TipoDeposito = UFunciones.Recort(dr1[12].ToString()),
                    NombreDestinatario = UFunciones.Recort(dr1[13].ToString()),
                    Glosa = UFunciones.Recort(dr1[14].ToString()),
                    Originador = UFunciones.Recort(dr1[15].ToString()),
                    OriginadorAch = UFunciones.Recort(dr1[16].ToString()),
                    CiudadOrigen = UFunciones.Recort(dr1[17].ToString()),
                    Debito = UFunciones.RecortDec(dr1[18].ToString()),
                    Credito = UFunciones.RecortDec(dr1[19].ToString()),
                    Saldo = UFunciones.RecortDec(dr1[20].ToString()),
                    Cuenta = Identificador,
                    EstadoConciliado = "REG",
                    Usuario = NombreUsuario,
                    NombreArchivo = NombreNormalizado,
                    FechaSubido = FechaActual
                };
                await Context.FoExtractoBmsc99s.AddAsync(model);
                // Guardado de cambios por lote
                if (contador % numeroLote == 0)
                {
                    try
                    {
                        _ = await Context.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        // DbUpdateConcurrencyException
                        throw;
                    }
                }
                contador++;
            }
            try
            {
                _ = await Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                // DbUpdateConcurrencyException
                throw;
            }
            // Cargado a tabla oficial
            await Context.Database.ExecuteSqlInterpolatedAsync($"EXECUTE [dbo].[Aprueba_BMSC]");
        }
    }
}
