﻿using SofiaNetCore.Data;
using System.Threading.Tasks;

namespace SofiaNetCore.Services.Strategy
{
    public interface IExcelStrategy
    {
        public Task UploadAsync(string RutaExcel, AppDbContext Context, IFtpServer ftpServer, string Identificador, string NombreNormalizado, string NombreUsuario);
    }
}
