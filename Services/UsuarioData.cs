﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Models;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofiaNetCore.Services
{
    public class UsuarioData : IUsuarioData
    {
        private readonly AppDbContext _db;
        public string? Usr_codigo { get; set; }
        public string? Usr_nombres { get; set; }
        public string? Usr_primer_apellido { get; set; }
        public string? Usr_segundo_apellido { get; set; }
        public UsuarioData(AppDbContext dbContext)
        {
            _db = dbContext;
        }
        private static string Decrypt(string pass)
        {
            return Encoding.ASCII.GetBytes(pass).Select(x => char.ConvertFromUtf32(x + 5)).Aggregate("", (ac, x) => ac + x);
        }
        public async Task<bool> Login(string user, string pass)
        {
            string passDecript = Decrypt(pass);
            GcUsuario? result = await _db.GcUsuarios.AsNoTracking().Where(m => m.UsrCodigo == user && m.UsrClave == passDecript).FirstOrDefaultAsync();
            if (result == null)
            {
                return false;
            }
            Usr_codigo = result.UsrCodigo;
            Usr_nombres = result.UsrNombres;
            Usr_primer_apellido = result.UsrPrimerApellido;
            Usr_segundo_apellido = result.UsrSegundoApellido;
            return true;
        }
    }
}
