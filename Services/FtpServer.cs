﻿using FluentFTP;
using System;
using System.IO;
using System.Security.Authentication;

namespace SofiaNetCore.Services
{
    public class FtpServer : IFtpServer, IDisposable
    {
        private readonly string _host;
        private readonly string _user;
        private readonly string _password;
        private readonly int _port = 21;
        private FtpClient? _ftp;
        private readonly FtpConfig _config = new()
        {
            EncryptionMode = FtpEncryptionMode.Auto,
            SslProtocols = SslProtocols.Tls12,
            ValidateAnyCertificate = true,
            DataConnectionType = FtpDataConnectionType.PASV,
            RetryAttempts = 3,
            ConnectTimeout = 10000
        };
        public FtpServer(string? host, string? user, string? password)
        {
            _host = host ?? "";
            _user = user ?? "";
            _password = password ?? "";
        }

        public void Respaldar(string ruta, string user, string ambiente)
        {
            string rutaD = @$"/{ambiente}/{user}/";
            _ftp = new FtpClient(_host, _user, _password, _port, _config);
            try
            {
                _ftp.Connect();
                _ftp.CreateDirectory(rutaD);
                _ftp.UploadFile(ruta, rutaD + $"{DateTime.Now:yyyyMMdd_HHmmss}_" + Path.GetFileName(ruta), FtpRemoteExists.Overwrite, false, FtpVerify.Retry);
                _ftp.Disconnect();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            _ftp?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
