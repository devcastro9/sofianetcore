﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Models;

namespace SofiaNetCore.Data
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public virtual DbSet<AuxiliarAsistencia99> AuxiliarAsistencia99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBcp> FoExtractoBcps { get; set; } = null!;
        public virtual DbSet<FoExtractoBcp99> FoExtractoBcp99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBmsc202101> FoExtractoBmsc202101s { get; set; } = null!;
        public virtual DbSet<FoExtractoBmsc99> FoExtractoBmsc99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBsol> FoExtractoBsols { get; set; } = null!;
        public virtual DbSet<FoExtractoBsol99> FoExtractoBsol99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBunion202101> FoExtractoBunion202101s { get; set; } = null!;
        public virtual DbSet<FoExtractoBunion99> FoExtractoBunion99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBnb> FoExtractoBnbs { get; set; } = null!;
        public virtual DbSet<FoExtractoBnb99> FoExtractoBnb99s { get; set; } = null!;
        public virtual DbSet<FoExtractoBga> FoExtractoBgas { get; set; } = null!;
        public virtual DbSet<FoExtractoBga99> FoExtractoBga99s { get; set; } = null!;
        public virtual DbSet<FvExtractoCuenta> FvExtractoCuentas { get; set; } = null!;
        public virtual DbSet<GcUsuario> GcUsuarios { get; set; } = null!;
        public virtual DbSet<RcPlanillaGrupo> RcPlanillaGrupos { get; set; } = null!;
        public virtual DbSet<RoControlAsistencium> RoControlAsistencia { get; set; } = null!;
        public virtual DbSet<RvEquipoAsistencium> RvEquipoAsistencia { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Metodo base
            base.OnModelCreating(modelBuilder);
            // Api fluent
            modelBuilder.Entity<FoExtractoBcp>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBcp99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBmsc202101>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBmsc99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBsol>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBsol99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBunion202101>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();

                entity.Property(e => e.FechaSubido).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<FoExtractoBunion99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBnb>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBnb99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBga>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FoExtractoBga99>(entity =>
            {
                entity.Property(e => e.EstadoConciliado).IsFixedLength();
            });

            modelBuilder.Entity<FvExtractoCuenta>(entity =>
            {
                entity.ToView("fv_extracto_cuentas");
            });

            modelBuilder.Entity<GcUsuario>(entity =>
            {
                entity.Property(e => e.EstadoCodigo).IsFixedLength();
            });

            modelBuilder.Entity<RcPlanillaGrupo>(entity =>
            {
                entity.Property(e => e.EstadoCodigo).IsFixedLength();
            });

            modelBuilder.Entity<RoControlAsistencium>(entity =>
            {
                entity.Property(e => e.EstadoRegistro).IsFixedLength();

                entity.Property(e => e.IngSal).IsFixedLength();

                entity.Property(e => e.Turno).IsFixedLength();

                entity.Property(e => e.Turno2).IsFixedLength();
            });

            modelBuilder.Entity<RvEquipoAsistencium>(entity =>
            {
                entity.ToView("rv_equipo_asistencia");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
