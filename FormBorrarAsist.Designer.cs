﻿namespace SofiaNetCore
{
    partial class FormBorrarAsist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IconExit = new FontAwesome.Sharp.IconButton();
            this.dtAsistencia = new System.Windows.Forms.DateTimePicker();
            this.ButtonBorrar = new FontAwesome.Sharp.IconButton();
            this.ComboPlanilla = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // IconExit
            // 
            this.IconExit.FlatAppearance.BorderSize = 0;
            this.IconExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconExit.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.IconExit.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.IconExit.IconColor = System.Drawing.Color.White;
            this.IconExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconExit.IconSize = 30;
            this.IconExit.Location = new System.Drawing.Point(708, 12);
            this.IconExit.Name = "IconExit";
            this.IconExit.Size = new System.Drawing.Size(30, 30);
            this.IconExit.TabIndex = 7;
            this.IconExit.UseVisualStyleBackColor = true;
            this.IconExit.Click += new System.EventHandler(this.IconExit_Click);
            // 
            // dtAsistencia
            // 
            this.dtAsistencia.CalendarFont = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtAsistencia.CustomFormat = " MM / yyyy";
            this.dtAsistencia.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtAsistencia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAsistencia.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dtAsistencia.Location = new System.Drawing.Point(477, 141);
            this.dtAsistencia.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtAsistencia.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtAsistencia.Name = "dtAsistencia";
            this.dtAsistencia.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtAsistencia.Size = new System.Drawing.Size(127, 30);
            this.dtAsistencia.TabIndex = 8;
            this.dtAsistencia.Value = new System.DateTime(2011, 12, 26, 15, 32, 0, 0);
            // 
            // ButtonBorrar
            // 
            this.ButtonBorrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ButtonBorrar.FlatAppearance.BorderSize = 0;
            this.ButtonBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBorrar.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ButtonBorrar.ForeColor = System.Drawing.Color.White;
            this.ButtonBorrar.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.ButtonBorrar.IconColor = System.Drawing.Color.White;
            this.ButtonBorrar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonBorrar.IconSize = 30;
            this.ButtonBorrar.Location = new System.Drawing.Point(305, 250);
            this.ButtonBorrar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ButtonBorrar.Name = "ButtonBorrar";
            this.ButtonBorrar.Size = new System.Drawing.Size(140, 50);
            this.ButtonBorrar.TabIndex = 9;
            this.ButtonBorrar.Text = "BORRAR";
            this.ButtonBorrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonBorrar.UseVisualStyleBackColor = false;
            this.ButtonBorrar.Click += new System.EventHandler(this.ButtonBorrar_Click);
            // 
            // ComboPlanilla
            // 
            this.ComboPlanilla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ComboPlanilla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboPlanilla.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComboPlanilla.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ComboPlanilla.ForeColor = System.Drawing.Color.White;
            this.ComboPlanilla.FormattingEnabled = true;
            this.ComboPlanilla.Location = new System.Drawing.Point(115, 140);
            this.ComboPlanilla.Margin = new System.Windows.Forms.Padding(0);
            this.ComboPlanilla.Name = "ComboPlanilla";
            this.ComboPlanilla.Size = new System.Drawing.Size(282, 31);
            this.ComboPlanilla.TabIndex = 10;
            // 
            // FormBorrarAsist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(750, 550);
            this.Controls.Add(this.ComboPlanilla);
            this.Controls.Add(this.ButtonBorrar);
            this.Controls.Add(this.dtAsistencia);
            this.Controls.Add(this.IconExit);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBorrarAsist";
            this.Text = "FormBorrarAsist";
            this.Load += new System.EventHandler(this.FormBorrarAsist_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton IconExit;
        private System.Windows.Forms.DateTimePicker dtAsistencia;
        private FontAwesome.Sharp.IconButton ButtonBorrar;
        private System.Windows.Forms.ComboBox ComboPlanilla;
    }
}