﻿using Microsoft.Extensions.DependencyInjection;
using SofiaNetCore.Services;
using System;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormMain : Form
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IUsuarioData _user;
        public FormMain(IServiceProvider serviceProv, IUsuarioData user)
        {
            InitializeComponent();
            _serviceProvider = serviceProv;
            _user = user;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void IconMin_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void BtnImportarExtracto_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelSubMenu1);
            panelSubMenu2.Visible = false;
        }
        private void BtnRegistroAsistencia_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panelSubMenu2);
            panelSubMenu1.Visible = false;
        }
        private static void ShowSubMenu(Panel pnl)
        {
            pnl.Visible = pnl.Visible != true;
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            // Submenu 1 no visible
            panelSubMenu1.Visible = false;
            // Submenu 2 no visible
            panelSubMenu2.Visible = false;
            // Carga de datos de usuario
            LblNombre.Text = _user.Usr_nombres;
            LblApellido.Text = $"{_user.Usr_primer_apellido} {_user.Usr_segundo_apellido}";
            // Seguridad: Authorization
            // Caso RRHH:
            //BtnImportarExtracto.Visible = false;
            // Caso Tesoreria:
            BtnRegistroAsistencia.Visible = false;
        }
        private Form? ActiveFrm = null;
        private void OpenChildFrm(Form ChildForm)
        {
            ActiveFrm?.Close();
            ActiveFrm = ChildForm;
            ChildForm.TopLevel = false;
            ChildForm.FormBorderStyle = FormBorderStyle.None;
            ChildForm.Dock = DockStyle.Fill;
            PanelContenedor.Controls.Add(ChildForm);
            PanelContenedor.Tag = ChildForm;
            ChildForm.BringToFront();
            ChildForm.Show();
        }
        private void ButtonExtractoBanco_Click(object sender, EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormBanco>());
            //OpenChildFrm(new FormBanco());
        }
        private void ButtonExtractoBorrar_Click(object sender, EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormBorrar>());
            //OpenChildFrm(new FormBorrar());
        }
        private void ButtonImportarAsistencia_Click(object sender, EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormAsistencia>());
            //OpenChildFrm(new FormAsistencia());
        }
        private void ButtonBorrarAsistencia_Click(object sender, EventArgs e)
        {
            OpenChildFrm(_serviceProvider.GetRequiredService<FormBorrarAsist>());
            //OpenChildFrm(new FormBorrarAsist());
        }
    }
}
