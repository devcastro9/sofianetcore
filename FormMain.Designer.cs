﻿namespace SofiaNetCore
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.panelMenuLateral = new System.Windows.Forms.Panel();
            this.panelSubMenu2 = new System.Windows.Forms.Panel();
            this.ButtonBorrarAsistencia = new FontAwesome.Sharp.IconButton();
            this.ButtonImportarAsistencia = new FontAwesome.Sharp.IconButton();
            this.BtnRegistroAsistencia = new FontAwesome.Sharp.IconButton();
            this.panelSubMenu1 = new System.Windows.Forms.Panel();
            this.ButtonExtractoBorrar = new FontAwesome.Sharp.IconButton();
            this.ButtonExtractoBanco = new FontAwesome.Sharp.IconButton();
            this.BtnImportarExtracto = new FontAwesome.Sharp.IconButton();
            this.panelUser = new System.Windows.Forms.Panel();
            this.LblApellido = new System.Windows.Forms.Label();
            this.LblNombre = new System.Windows.Forms.Label();
            this.IconUser = new FontAwesome.Sharp.IconPictureBox();
            this.PanelMenuSuperior = new System.Windows.Forms.Panel();
            this.IconExit = new FontAwesome.Sharp.IconButton();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.pictureMain = new System.Windows.Forms.PictureBox();
            this.IconMin = new FontAwesome.Sharp.IconButton();
            this.panelMenuLateral.SuspendLayout();
            this.panelSubMenu2.SuspendLayout();
            this.panelSubMenu1.SuspendLayout();
            this.panelUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IconUser)).BeginInit();
            this.PanelMenuSuperior.SuspendLayout();
            this.PanelContenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMain)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenuLateral
            // 
            this.panelMenuLateral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelMenuLateral.Controls.Add(this.panelSubMenu2);
            this.panelMenuLateral.Controls.Add(this.BtnRegistroAsistencia);
            this.panelMenuLateral.Controls.Add(this.panelSubMenu1);
            this.panelMenuLateral.Controls.Add(this.BtnImportarExtracto);
            this.panelMenuLateral.Controls.Add(this.panelUser);
            this.panelMenuLateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenuLateral.Location = new System.Drawing.Point(0, 0);
            this.panelMenuLateral.Name = "panelMenuLateral";
            this.panelMenuLateral.Size = new System.Drawing.Size(250, 600);
            this.panelMenuLateral.TabIndex = 0;
            // 
            // panelSubMenu2
            // 
            this.panelSubMenu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelSubMenu2.Controls.Add(this.ButtonBorrarAsistencia);
            this.panelSubMenu2.Controls.Add(this.ButtonImportarAsistencia);
            this.panelSubMenu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu2.Location = new System.Drawing.Point(0, 293);
            this.panelSubMenu2.Name = "panelSubMenu2";
            this.panelSubMenu2.Size = new System.Drawing.Size(250, 93);
            this.panelSubMenu2.TabIndex = 5;
            // 
            // ButtonBorrarAsistencia
            // 
            this.ButtonBorrarAsistencia.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonBorrarAsistencia.FlatAppearance.BorderSize = 0;
            this.ButtonBorrarAsistencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBorrarAsistencia.ForeColor = System.Drawing.Color.White;
            this.ButtonBorrarAsistencia.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.ButtonBorrarAsistencia.IconColor = System.Drawing.Color.White;
            this.ButtonBorrarAsistencia.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonBorrarAsistencia.IconSize = 35;
            this.ButtonBorrarAsistencia.Location = new System.Drawing.Point(0, 45);
            this.ButtonBorrarAsistencia.Margin = new System.Windows.Forms.Padding(0);
            this.ButtonBorrarAsistencia.Name = "ButtonBorrarAsistencia";
            this.ButtonBorrarAsistencia.Size = new System.Drawing.Size(250, 45);
            this.ButtonBorrarAsistencia.TabIndex = 1;
            this.ButtonBorrarAsistencia.Text = "Borrar";
            this.ButtonBorrarAsistencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonBorrarAsistencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonBorrarAsistencia.UseVisualStyleBackColor = true;
            this.ButtonBorrarAsistencia.Click += new System.EventHandler(this.ButtonBorrarAsistencia_Click);
            // 
            // ButtonImportarAsistencia
            // 
            this.ButtonImportarAsistencia.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonImportarAsistencia.FlatAppearance.BorderSize = 0;
            this.ButtonImportarAsistencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonImportarAsistencia.ForeColor = System.Drawing.Color.White;
            this.ButtonImportarAsistencia.IconChar = FontAwesome.Sharp.IconChar.FileExcel;
            this.ButtonImportarAsistencia.IconColor = System.Drawing.Color.White;
            this.ButtonImportarAsistencia.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonImportarAsistencia.IconSize = 35;
            this.ButtonImportarAsistencia.Location = new System.Drawing.Point(0, 0);
            this.ButtonImportarAsistencia.Margin = new System.Windows.Forms.Padding(0);
            this.ButtonImportarAsistencia.Name = "ButtonImportarAsistencia";
            this.ButtonImportarAsistencia.Size = new System.Drawing.Size(250, 45);
            this.ButtonImportarAsistencia.TabIndex = 0;
            this.ButtonImportarAsistencia.Text = "Importar";
            this.ButtonImportarAsistencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonImportarAsistencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonImportarAsistencia.UseVisualStyleBackColor = true;
            this.ButtonImportarAsistencia.Click += new System.EventHandler(this.ButtonImportarAsistencia_Click);
            // 
            // BtnRegistroAsistencia
            // 
            this.BtnRegistroAsistencia.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRegistroAsistencia.FlatAppearance.BorderSize = 0;
            this.BtnRegistroAsistencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRegistroAsistencia.ForeColor = System.Drawing.Color.White;
            this.BtnRegistroAsistencia.IconChar = FontAwesome.Sharp.IconChar.IdCard;
            this.BtnRegistroAsistencia.IconColor = System.Drawing.Color.White;
            this.BtnRegistroAsistencia.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnRegistroAsistencia.IconSize = 35;
            this.BtnRegistroAsistencia.Location = new System.Drawing.Point(0, 238);
            this.BtnRegistroAsistencia.Name = "BtnRegistroAsistencia";
            this.BtnRegistroAsistencia.Size = new System.Drawing.Size(250, 55);
            this.BtnRegistroAsistencia.TabIndex = 4;
            this.BtnRegistroAsistencia.Text = "Registro de asistencia";
            this.BtnRegistroAsistencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnRegistroAsistencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnRegistroAsistencia.UseVisualStyleBackColor = true;
            this.BtnRegistroAsistencia.Click += new System.EventHandler(this.BtnRegistroAsistencia_Click);
            // 
            // panelSubMenu1
            // 
            this.panelSubMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelSubMenu1.Controls.Add(this.ButtonExtractoBorrar);
            this.panelSubMenu1.Controls.Add(this.ButtonExtractoBanco);
            this.panelSubMenu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu1.Location = new System.Drawing.Point(0, 145);
            this.panelSubMenu1.Name = "panelSubMenu1";
            this.panelSubMenu1.Size = new System.Drawing.Size(250, 93);
            this.panelSubMenu1.TabIndex = 2;
            // 
            // ButtonExtractoBorrar
            // 
            this.ButtonExtractoBorrar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonExtractoBorrar.FlatAppearance.BorderSize = 0;
            this.ButtonExtractoBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonExtractoBorrar.ForeColor = System.Drawing.Color.White;
            this.ButtonExtractoBorrar.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.ButtonExtractoBorrar.IconColor = System.Drawing.Color.White;
            this.ButtonExtractoBorrar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonExtractoBorrar.IconSize = 35;
            this.ButtonExtractoBorrar.Location = new System.Drawing.Point(0, 45);
            this.ButtonExtractoBorrar.Margin = new System.Windows.Forms.Padding(0);
            this.ButtonExtractoBorrar.Name = "ButtonExtractoBorrar";
            this.ButtonExtractoBorrar.Size = new System.Drawing.Size(250, 45);
            this.ButtonExtractoBorrar.TabIndex = 1;
            this.ButtonExtractoBorrar.Text = "Borrar extracto";
            this.ButtonExtractoBorrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonExtractoBorrar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonExtractoBorrar.UseVisualStyleBackColor = true;
            this.ButtonExtractoBorrar.Click += new System.EventHandler(this.ButtonExtractoBorrar_Click);
            // 
            // ButtonExtractoBanco
            // 
            this.ButtonExtractoBanco.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonExtractoBanco.FlatAppearance.BorderSize = 0;
            this.ButtonExtractoBanco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonExtractoBanco.ForeColor = System.Drawing.Color.White;
            this.ButtonExtractoBanco.IconChar = FontAwesome.Sharp.IconChar.University;
            this.ButtonExtractoBanco.IconColor = System.Drawing.Color.White;
            this.ButtonExtractoBanco.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ButtonExtractoBanco.IconSize = 35;
            this.ButtonExtractoBanco.Location = new System.Drawing.Point(0, 0);
            this.ButtonExtractoBanco.Margin = new System.Windows.Forms.Padding(0);
            this.ButtonExtractoBanco.Name = "ButtonExtractoBanco";
            this.ButtonExtractoBanco.Size = new System.Drawing.Size(250, 45);
            this.ButtonExtractoBanco.TabIndex = 0;
            this.ButtonExtractoBanco.Text = "Extracto Banco";
            this.ButtonExtractoBanco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonExtractoBanco.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonExtractoBanco.UseVisualStyleBackColor = true;
            this.ButtonExtractoBanco.Click += new System.EventHandler(this.ButtonExtractoBanco_Click);
            // 
            // BtnImportarExtracto
            // 
            this.BtnImportarExtracto.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnImportarExtracto.FlatAppearance.BorderSize = 0;
            this.BtnImportarExtracto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnImportarExtracto.ForeColor = System.Drawing.Color.White;
            this.BtnImportarExtracto.IconChar = FontAwesome.Sharp.IconChar.FileExcel;
            this.BtnImportarExtracto.IconColor = System.Drawing.Color.White;
            this.BtnImportarExtracto.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.BtnImportarExtracto.IconSize = 35;
            this.BtnImportarExtracto.Location = new System.Drawing.Point(0, 90);
            this.BtnImportarExtracto.Name = "BtnImportarExtracto";
            this.BtnImportarExtracto.Size = new System.Drawing.Size(250, 55);
            this.BtnImportarExtracto.TabIndex = 3;
            this.BtnImportarExtracto.Text = "Importar Extracto";
            this.BtnImportarExtracto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnImportarExtracto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnImportarExtracto.UseVisualStyleBackColor = true;
            this.BtnImportarExtracto.Click += new System.EventHandler(this.BtnImportarExtracto_Click);
            // 
            // panelUser
            // 
            this.panelUser.Controls.Add(this.LblApellido);
            this.panelUser.Controls.Add(this.LblNombre);
            this.panelUser.Controls.Add(this.IconUser);
            this.panelUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUser.Location = new System.Drawing.Point(0, 0);
            this.panelUser.Name = "panelUser";
            this.panelUser.Size = new System.Drawing.Size(250, 90);
            this.panelUser.TabIndex = 0;
            // 
            // LblApellido
            // 
            this.LblApellido.AutoSize = true;
            this.LblApellido.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.LblApellido.ForeColor = System.Drawing.Color.White;
            this.LblApellido.Location = new System.Drawing.Point(80, 65);
            this.LblApellido.Name = "LblApellido";
            this.LblApellido.Size = new System.Drawing.Size(59, 19);
            this.LblApellido.TabIndex = 2;
            this.LblApellido.Text = "Nombre";
            this.LblApellido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblNombre
            // 
            this.LblNombre.AutoSize = true;
            this.LblNombre.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.LblNombre.ForeColor = System.Drawing.Color.White;
            this.LblNombre.Location = new System.Drawing.Point(81, 47);
            this.LblNombre.Name = "LblNombre";
            this.LblNombre.Size = new System.Drawing.Size(56, 19);
            this.LblNombre.TabIndex = 1;
            this.LblNombre.Text = "Usuario";
            this.LblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // IconUser
            // 
            this.IconUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.IconUser.IconChar = FontAwesome.Sharp.IconChar.UserAlt;
            this.IconUser.IconColor = System.Drawing.Color.White;
            this.IconUser.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconUser.IconSize = 25;
            this.IconUser.Location = new System.Drawing.Point(49, 53);
            this.IconUser.Name = "IconUser";
            this.IconUser.Size = new System.Drawing.Size(25, 25);
            this.IconUser.TabIndex = 0;
            this.IconUser.TabStop = false;
            // 
            // PanelMenuSuperior
            // 
            this.PanelMenuSuperior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.PanelMenuSuperior.Controls.Add(this.IconMin);
            this.PanelMenuSuperior.Controls.Add(this.IconExit);
            this.PanelMenuSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMenuSuperior.Location = new System.Drawing.Point(250, 0);
            this.PanelMenuSuperior.Name = "PanelMenuSuperior";
            this.PanelMenuSuperior.Size = new System.Drawing.Size(750, 50);
            this.PanelMenuSuperior.TabIndex = 1;
            // 
            // IconExit
            // 
            this.IconExit.FlatAppearance.BorderSize = 0;
            this.IconExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconExit.ForeColor = System.Drawing.Color.White;
            this.IconExit.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.IconExit.IconColor = System.Drawing.Color.White;
            this.IconExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconExit.IconSize = 30;
            this.IconExit.Location = new System.Drawing.Point(714, 15);
            this.IconExit.Name = "IconExit";
            this.IconExit.Size = new System.Drawing.Size(24, 24);
            this.IconExit.TabIndex = 0;
            this.IconExit.UseVisualStyleBackColor = true;
            this.IconExit.Click += new System.EventHandler(this.IconExit_Click);
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.PanelContenedor.Controls.Add(this.pictureMain);
            this.PanelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenedor.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PanelContenedor.Location = new System.Drawing.Point(250, 50);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(750, 550);
            this.PanelContenedor.TabIndex = 2;
            // 
            // pictureMain
            // 
            this.pictureMain.Image = ((System.Drawing.Image)(resources.GetObject("pictureMain.Image")));
            this.pictureMain.Location = new System.Drawing.Point(265, 143);
            this.pictureMain.Name = "pictureMain";
            this.pictureMain.Size = new System.Drawing.Size(220, 218);
            this.pictureMain.TabIndex = 0;
            this.pictureMain.TabStop = false;
            // 
            // IconMin
            // 
            this.IconMin.FlatAppearance.BorderSize = 0;
            this.IconMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IconMin.ForeColor = System.Drawing.Color.White;
            this.IconMin.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.IconMin.IconColor = System.Drawing.Color.White;
            this.IconMin.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.IconMin.IconSize = 30;
            this.IconMin.Location = new System.Drawing.Point(671, 12);
            this.IconMin.Name = "IconMin";
            this.IconMin.Size = new System.Drawing.Size(24, 24);
            this.IconMin.TabIndex = 1;
            this.IconMin.UseVisualStyleBackColor = true;
            this.IconMin.Click += new System.EventHandler(this.IconMin_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.PanelContenedor);
            this.Controls.Add(this.PanelMenuSuperior);
            this.Controls.Add(this.panelMenuLateral);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMain";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panelMenuLateral.ResumeLayout(false);
            this.panelSubMenu2.ResumeLayout(false);
            this.panelSubMenu1.ResumeLayout(false);
            this.panelUser.ResumeLayout(false);
            this.panelUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IconUser)).EndInit();
            this.PanelMenuSuperior.ResumeLayout(false);
            this.PanelContenedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenuLateral;
        private System.Windows.Forms.Panel panelUser;
        private System.Windows.Forms.Panel panelSubMenu1;
        private FontAwesome.Sharp.IconButton ButtonExtractoBorrar;
        private FontAwesome.Sharp.IconButton ButtonExtractoBanco;
        private System.Windows.Forms.Panel PanelMenuSuperior;
        private FontAwesome.Sharp.IconButton IconExit;
        private System.Windows.Forms.Panel PanelContenedor;
        private FontAwesome.Sharp.IconButton BtnImportarExtracto;
        private FontAwesome.Sharp.IconPictureBox IconUser;
        private System.Windows.Forms.Label LblApellido;
        private System.Windows.Forms.Label LblNombre;
        private System.Windows.Forms.PictureBox pictureMain;
        private System.Windows.Forms.Panel panelSubMenu2;
        private FontAwesome.Sharp.IconButton ButtonBorrarAsistencia;
        private FontAwesome.Sharp.IconButton ButtonImportarAsistencia;
        private FontAwesome.Sharp.IconButton BtnRegistroAsistencia;
        private FontAwesome.Sharp.IconButton IconMin;
    }
}