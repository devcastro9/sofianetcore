using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SofiaNetCore.Data;
using SofiaNetCore.Services;
using System;
using System.Windows.Forms;

namespace SofiaNetCore
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Doc https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            //Application.Run(new FormLogin());
            // Inyeccion de dependencias
            IHostBuilder builder = Host.CreateDefaultBuilder().ConfigureServices((context, services) =>
            {
                services.AddDbContext<AppDbContext>(options => options.UseSqlServer(context.Configuration.GetConnectionString("Wbase")));
                services.AddSingleton<FormLogin>();
                services.AddTransient<FormMain>();
                services.AddTransient<FormBanco>();
                services.AddTransient<FormBorrar>();
                services.AddTransient<FormAsistencia>();
                services.AddTransient<FormBorrarAsist>();
                services.AddScoped<IUsuarioData, UsuarioData>();
                services.AddScoped<IFtpServer>(sp => new FtpServer(context.Configuration.GetSection("Ftp")["Host"], context.Configuration.GetSection("Ftp")["User"], context.Configuration.GetSection("Ftp")["Password"]));
            });
            IHost host = builder.Build();
            using IServiceScope serviceScope = host.Services.CreateScope();
            IServiceProvider servicios = serviceScope.ServiceProvider;
            Application.Run(servicios.GetRequiredService<FormLogin>());
        }
    }
}