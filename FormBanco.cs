﻿using Microsoft.EntityFrameworkCore;
using SofiaNetCore.Data;
using SofiaNetCore.Services;
using SofiaNetCore.Services.Strategy;
using SofiaNetCore.Utility;
using System;
using System.Linq;
using System.Windows.Forms;

namespace SofiaNetCore
{
    public partial class FormBanco : Form
    {
        private readonly AppDbContext _db;
        private readonly IFtpServer _ftpServer;
        private readonly IUsuarioData _usuario;
        private bool estado = false;
        public FormBanco(AppDbContext appDb, IFtpServer ftpServer, IUsuarioData usuario)
        {
            InitializeComponent();
            _db = appDb;
            _ftpServer = ftpServer;
            _usuario = usuario;
        }
        private void IconExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void ButtonImportar_Click(object sender, EventArgs e)
        {
            try
            {
                string? Cuenta = ComboBancos.SelectedValue!.ToString();
                string? BancoCodigo = ObtenerBanco(Cuenta);
                if (BancoCodigo == null || Cuenta == null)
                {
                    MessageBox.Show("No se encontro el banco asociado a la cuenta bancaria.", "Problema", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                using OpenFileDialog archive = new();
                archive.Title = "Importar Excel - Extractos Bancarios";
                archive.InitialDirectory = @"C:\\";
                archive.Filter = "Excel 97-2003 (*.xls)|*.xls|Libro de Excel (*.xlsx)|*.xlsx";
                archive.FilterIndex = 0;
                archive.RestoreDirectory = true;
                if (archive.ShowDialog() == DialogResult.OK)
                {
                    ButtonImportar.Enabled = false;
                    if (!string.IsNullOrEmpty(archive.FileName))
                    {
                        string NombreNormalizado = UFunciones.NormalizarExtracto(PorMes.Checked, dtExtracto.Value, Cuenta);
                        IExcelStrategy ies = BancoCodigo switch
                        {
                            "BMSC" => new ExtractoBMSC(),
                            "BUN" => new ExtractoBUNION(),
                            "BCP" => new ExtractoBCP(),
                            "BNB" => new ExtractoBNB(),
                            "BGA" => new ExtractoBGA(),
                            "BSOL" => new ExtractoBSOL(),
                            _ => new ExtractoBMSC(),
                        };
                        // Creacion del objeto Context para el patron Strategy
                        ExcelContext oExcel = new(ies);
                        await oExcel.CargarExcelAsync(archive.FileName, _db, _ftpServer, Cuenta, NombreNormalizado, _usuario.Usr_codigo ?? "ADMIN");
                        ActualizarDatos();
                        MessageBox.Show("Archivo importado correctamente.", "Carga de archivo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Ocurrio un problema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ButtonImportar.Enabled = true;
            }
        }
        private void FormBanco_Load(object sender, EventArgs e)
        {
            // Cargar datos
            DataExtractos.DataSource = null;
            // Objeto de carga de datos
            ComboBancos.DataSource = _db.FvExtractoCuentas.AsNoTracking().ToList();
            ComboBancos.DisplayMember = "Descripcion";
            ComboBancos.ValueMember = "cuenta";
            ComboBancos.SelectedIndex = 0;
            // Valores por defecto del DateTimePicker
            dtExtracto.CustomFormat = PorMes.Checked ? " MM / yyyy" : " dd/MM/yyyy";
            dtExtracto.Value = DateTime.Now;
            //ActualizarDatos();
            estado = true;
        }

        private string? ObtenerBanco(string? cuenta)
        {
            if (!estado)
            {
                return null;
            }
            if (cuenta == null)
            {
                return null;
            }
            string? BancoCodigo = (from bco in _db.FvExtractoCuentas where bco.Cuenta == cuenta select bco.Banco).FirstOrDefault();
            if (BancoCodigo == null)
            {
                return null;
            }
            return BancoCodigo;
        }
        private async void ActualizarDatos()
        {
            if (!estado)
            {
                return;
            }
            string? Cuenta = ComboBancos.SelectedValue!.ToString();
            string? Banco = ObtenerBanco(Cuenta);
            DataExtractos.DataSource = null;
            if (Cuenta == null || Banco == null)
            {
                return;
            }
            DateTime fecha = dtExtracto.Value;
            if (PorMes.Checked)
            {
                DataExtractos.DataSource = Banco switch
                {
                    "BMSC" => await _db.FoExtractoBmsc202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.FechaTransaccion!.Value.Year == fecha.Year && m.FechaTransaccion!.Value.Month == fecha.Month).ToListAsync(),
                    "BUN" => await _db.FoExtractoBunion202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month).ToListAsync(),
                    "BCP" => await _db.FoExtractoBcps.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month).ToListAsync(),
                    "BNB" => await _db.FoExtractoBnbs.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month).ToListAsync(),
                    "BGA" => await _db.FoExtractoBgas.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month).ToListAsync(),
                    "BSOL" => await _db.FoExtractoBsols.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month).ToListAsync(),
                    _ => null,
                };
            }
            else
            {
                DataExtractos.DataSource = Banco switch
                {
                    "BMSC" => await _db.FoExtractoBmsc202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.FechaTransaccion!.Value.Year == fecha.Year && m.FechaTransaccion!.Value.Month == fecha.Month && m.FechaTransaccion!.Value.Day == fecha.Day).ToListAsync(),
                    "BUN" => await _db.FoExtractoBunion202101s.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month && m.Fecha!.Value.Day == fecha.Day).ToListAsync(),
                    "BCP" => await _db.FoExtractoBcps.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month && m.Fecha!.Value.Day == fecha.Day).ToListAsync(),
                    "BNB" => await _db.FoExtractoBnbs.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month && m.Fecha!.Value.Day == fecha.Day).ToListAsync(),
                    "BGA" => await _db.FoExtractoBgas.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month && m.Fecha!.Value.Day == fecha.Day).ToListAsync(),
                    "BSOL" => await _db.FoExtractoBsols.AsNoTracking().Where(m => m.Cuenta == Cuenta && m.Fecha!.Value.Year == fecha.Year && m.Fecha!.Value.Month == fecha.Month && m.Fecha!.Value.Day == fecha.Day).ToListAsync(),
                    _ => null,
                };
            }
        }
        private void PorMes_CheckedChanged(object sender, EventArgs e)
        {
            dtExtracto.CustomFormat = " MM / yyyy";
            if (PorMes.Checked)
            {
                ActualizarDatos();
            }
        }
        private void PorDia_CheckedChanged(object sender, EventArgs e)
        {
            dtExtracto.CustomFormat = " dd/MM/yyyy";
            if (PorDia.Checked)
            {
                ActualizarDatos();
            }
        }
        private void dtExtracto_ValueChanged(object sender, EventArgs e)
        {
            ActualizarDatos();
        }

        private void ComboBancos_SelectedValueChanged(object sender, EventArgs e)
        {
            ActualizarDatos();
        }
    }
}
